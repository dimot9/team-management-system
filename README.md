# Team Management System

**Football team management system.** Web application that connects coaches, players and their parents. The system is a center of all team data and information. It delivers a possibility to apply a modern trends of training for coaches. Players has another opportunity to enhance their skills and their parents get better overview about children’s and whole team activities.

It's my bachelor's work being composed in Czech Technical University in Prague - Faculty of electrical engineering.

## Stack

Web app's architecture is quite simple. It has its own relational database, then some API and user interface (Frontend) itself.

API service process and delivers all of a neccessary data and persists them in the database.

Frontend gets data from the API and let user interact with them.

So we have three services, which communicate with each other.
- Database
- API
- Frontend

![app-architecture](https://gitlab.com/dimot9/team-management-system/-/raw/master/bachelors-work/latex/images/app-architecture.png)

### Database

- relational
- mariadb

### API

- NodeJS - using ExpressJS framework
- GraphQL - using Apollo Server library
- Typescript - let's keep it typed :) 


### Frontend

- VueJS - using NuxtJS framework
- Tailwind CSS

## Development

### Requirments

- [nodejs](https://nodejs.org/en/download/)
- [docker](https://docs.docker.com/desktop/)
- [docker-compose](https://docs.docker.com/compose/install/)
- [yarn](https://yarnpkg.com/getting-started/install)
    - or if you prefer **npm** use it, but global package.json scripts won't work. Or you can rewrite them, instead of `yarn dev` use `npm run dev`

### Run database

There are 2 ways:

1. Run separately using 

    `yarn start:db`

2. Run alongside with API 

    `yarn dev:api`

### Run API

`yarn dev:api`

### Run Frontend (web)

`yarn dev:web`

## Testing

### Run API tests
```shell script
cd source/api
yarn test
```

## Structure

```
.
├── bachelors-work
│   ├── attachments
│   ├── latex
│   └── semestrial-work
├── dev-ops
└── source
    ├── api
    ├── db
    └── web
```

Bachelors work folder includes latex source of the text, attachments and semestrial work folder, which was preparation before bachelors work.

Dev ops folder includes some helper bash scripts (for example for running DB)

Source folder includes three subfolders each represents the one of the system services.

- api = API
- db = Database
- web = Frontend

Api and web have its own package,json files with their own dependencies and scripts.

Db has two docker-compose config files. One for running **development database** container and second for the **test database** container.