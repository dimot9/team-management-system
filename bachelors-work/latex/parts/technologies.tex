V této kapitole se práce zabývá technologiemi a nástroji, které se během implementace aplikace používají. Uvádí se jakou mají roli a stručně se popisuje proč jim byla dána přednost před jinými nástroji a technologiemi.

Aplikace se skládá ze tří spolu komunikujících a izolovaných jednotek (služeb). Těmi jsou - API\nomenclature{API}{Application Programming Interface}, databáze a pak samotné webově uživatelské rozhraní, které se dále označuje jako frontend aplikace. Na \textbf{obrázku \ref{fig:application_architecture}} je zobrazeno, jak spolu jednotlivé služby komunikují. Služba API se stará o řídící logiku a datový model. Dále ukládá data do databáze. Služba frontend vytváři požadavky, které posílá na API a odpověď podle potřeby vykresluje uživateli. Jedná se o architekturu orientovanou na služby (SOA\nomenclature{SOA}{Service Oriented Architecture}), která zaručí znovupoužitelnost, nezávislost a skladatelnost systému. Systém může být rozšířen o další služby, což zvyšuje příležitosti pro zlepšení systému. Navíc například API může být v budoucnu klíčovou službou pro mobilní aplikaci. Webová a mobilní aplikace budou pracovat se stejným zdrojem dat a budou tak navzájem zastupitelné.

\begin{figure}[!htp]
    \begin{center}
    \includegraphics[width=\textwidth]{images/app-architecture.png}
    \end{center}
    \caption{Graf architektury aplikace}
    \label{fig:application_architecture}
\end{figure}

\subsection{Databáze}

Relační databáze nám umožňuje velice jednoduše vytvářet vazby mezi entitami. Na rozdíl od dokumentové databáze nám je schopná zaručit integritu dat. Dokumentová databáze v tomto ohledu není příliš vhodná pro složitější webové aplikace. Rychlost čtení dat, která je ve většině případů až dvakrát rychlejší než u relačních databází\cite{db-comparison}, je v této aplikaci rozumné obětovat ve prospěch zajištění integrity na datbázové úrovni. Na druhou stranu vazby v tomto systému nejsou natolik komplikované, kdy by bylo efektivnější využít databázi grafovou.

MySQL\cite{mysql} je databázový server optimalizovaný spíše pro webové aplikace, při více komplexních dotazech s obrovským obsahem dat ztrácí na výkonnosti a efiktivitě. Software, který ukládá a čte obrovské množství dat a provádí nad těmito daty všemožné analýzy, bude spíše využívat server PostgresSQL\cite{postgres-vs-mysql}. To však není případ této aplikace. MariaDB\cite{mariadb-about} je - z hlediska výkonnosti a bezpečnosti - optimalizovaná varianta MySQL. Navíc vývoj tohoto softwaru je veřejně otevřený. Kdokoliv může přispět opravou v systému, takový přístup přispívá k rychlejšímu odstranění bezpečnostních rizik.

Pro daný systém byla vybrána \textbf{relační databáze} a konkrétně server \textbf{Maria\-DB}. MariaDB je otevřený software vyvýjený původními vývojaři konkureční platformy MySQL. MariaDB je odnož MySQL, což dělá případnou migraci mezi těmito systémy značně jednodušší.

\subsection{API}

Pro API bylo vybráno \textbf{NodeJS}\cite{nodejs}, konkrétně bude použit framework ExpressJS\cite{expressjs}. NodeJS používá jednovláknový model zpracování událostí v cyklu, díky tomu je server schopen odpovídat, aniž by blokoval jiné události. Zpracování velkého množství požadavků najednou do jisté míry nijak nezpomaluje systém. Popřípadě aplikace implementované pomocí této technologie jsou velmi dobře škálovatelné. 

Existuje velké množství alternativ pro vývoj webových aplikací. Ve většině případů je to jen otázkou osobní preference. Například Spring Boot Java framework je osvědčenou a stabilní technologií. Výhodou může být typovost nebo ověřenost technologie díky její dlouhodobé dominanci. Typovost je ale možné v javascriptových technologiích zajistit jazykem Typescript\cite{typescript}, který je technologií NodeJS plně podporován. Navíc umožňuje objektově orientované programování. Sázím na trend vývoje technologií\cite{programming-trends}, udržitelnost a přepoužitelnost zdrojového kódu. Další výhodou javascriptové technologie je jednoduché a rychlé spravování balíků (pomocí NPM modulů\cite{npm}), které vývoj zefektivní.

Dalším důvodem výběru NodeJS je kompatibilita s technologií \\\textbf{GraphQL\cite{graphql}}. Ta nahrazuje klasický přístup \nomenclature{REST}{Representational state transfer}. Při použití GraphQL klient nemusí vytvářet několik dotazů pro získání příbuzných dat, stačí jeden dotaz, který vrátí veškerá potřebná data\cite{rest-vs-graphql}.

Automatickou konverzi mezi Typescript objekty a relační databází zajistí \textbf{TypeORM\cite{typeorm}}.

\subsection{Frontend}

Frontend služba bude postavena na frameworku \textbf{NuxtJS}\cite{nuxtjs}. Za serverovou část odpovídá opět NodeJS a klientská část je tvořena pomocí \textbf{VueJS}\cite{vuejs}. To umožňují vytváření přepoužitelných komponent, které jsou navíc interpretovány na straně serveru. Umožňí to optimalizaci pro vyhledávače, která prospěje k zviditelnění celého projektu.

Existuje velký počet volně otevřených javascriptových frameworků pro vývoj klientských aplikací. Momentálně nejpopulárnějšími jsou React, Angular a Vue\footnote{Na portálu \textit{github.com} je možné dohledat oficiální repozitáře jednotlivých frameworků a jejich popularitu mezi vývojáři.}. Není možné říci, který z nich je lepší nebo horší pro vývoj konkrétní aplikace. Každý z nich má jiný způsob zápisu a přístup k tvorbě šablon. VueJS bylo vybráno z důvodu velikosti celé knihovny, která má celkově 80 KB, což je v porovnání s dvěma konkurenty nejméně\cite{vue-react-angular}. Dále z důvodu velké popularity a jednoduchému a rychlému protoypování funkčních komponent.

Ke stylování vzhledu se používá CSS framework s názvem \textbf{Tailwind}\cite{tailwind}. Opět hlavní výhodou je rychlé prototypování vzhledu komponent pomocí účelových tříd. Účelové třídy jsou globální, pokud změníte hodnotu jedné z nich, změna se aplikuje všude, kde byla použita. Spojení komponent a účelových tříd může vést k velmi efektivnímu programování a přehlednému kódu, ve kterém se zobrazená stránka skládá z připravených komponent.

\subsection{Externí služby}

Aby si uživatelé mohli obnovit heslo nebo pozvat do týmu nového člena, posílá systém e-mailvé zprávy. Během vývoje aplikace se využívá služba Mailtrap\cite{mailtrap}, která odchytává odeslané e-maily z aplikace, a tak nikdy nedojdou adresátovi. Vývojář může tímto způsobem testovat funkcionalitu využívající odesílání mailů.

V případě ostrého provozu je potřeba systém napojit na externí mailový server, který bude schopen odeslané zprávy ze systému doručit adresátovi.