#!/usr/bin/env bash

# Exit script on first error
set -e

## NORMALIZE PATH
#########################################################################################################################
# Get current dir (from call)
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  TARGET="$(readlink "$SOURCE")"
  if [[ $TARGET == /* ]]; then
    SOURCE="$TARGET"
  else
    DIR="$( dirname "$SOURCE" )"
    SOURCE="$DIR/$TARGET" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
  fi
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
#########################################################################################################################
## NORMALIZE PATH - END

# Export the vars in .env into your shell:
export $(egrep -v '^#' ${DIR}/../.env | xargs)

# start dev DB
docker-compose \
  --project-name=tms-db \
  --file ${DIR}/../source/db/docker-compose.yml \
  up \
  -d \
  --force-recreate
