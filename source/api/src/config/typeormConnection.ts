import entities from '../entities'
import { createConnection, Connection} from 'typeorm'
import Config from "../loaders/config";

export default (logging = false): Promise<Connection> => {
  return createConnection({
    type: 'mariadb',
    host: Config.db.host,
    port: Config.db.port,
    username: Config.db.user,
    password: Config.db.password,
    database: Config.db.database,
    entities
  })
}
