import typeormConnection from '../../config/typeormConnection'
import { highlight } from 'cli-highlight'
import { printYellow, printRed, printGreen } from '../utils'

/* tslint:disable:no-console */
export default {
  cmd: `db:check`,
  descrition: `Check DB schema and print database alter script`,
  async handler(cmd: string, args: [string]) {
    try {
      const connection = await typeormConnection()
      const sqlInMemory = await connection.driver.createSchemaBuilder().log()
      if (sqlInMemory.upQueries.length === 0) {
        printGreen(
          'Your schema is up to date - there are no queries to be executed by schema syncronization.'
        )
      } else {
        const lengthSeparators = String(sqlInMemory.upQueries.length)
          .split('')
          .map((char) => '-')
          .join('')
        printYellow(
          `----------------------------------------------${lengthSeparators}`
        )
        printYellow(
          `-- Schema syncronization will execute following sql queries (${sqlInMemory.upQueries.length}):`
        )
        printYellow(
          `----------------------------------------------${lengthSeparators}`
        )

        sqlInMemory.upQueries.forEach((upQuery: any) => {
          let sqlString = upQuery.query
          sqlString = sqlString.trim()
          sqlString = sqlString.substr(-1) === ';' ? sqlString : sqlString + ';'
          console.log(highlight(sqlString))
        })
      }
      await connection.close()
    } catch (e) {
      printRed('☠️  Unable to connect to the database:', e)
    }
  }
}
