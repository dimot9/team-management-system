import typeormConnection from '../../config/typeormConnection'
// import datafixturesLoader from '../../loaders/datafixtures'
import { printGreen, printRed } from '../utils'

export default {
  cmd: `db:reset`,
  descrition: `Remove and recreate database schema with all data. New seeds/datafixtures will be loaded.`,
  async handler(cmd: string, args: [string]) {
    try {
      const connection = await typeormConnection()

      await connection.dropDatabase()
      await connection.synchronize()

      // await datafixturesLoader()

      await connection.close()
      printGreen('Schema reset finished successfully with new seeds')

    } catch (e) {
      printRed('☠️  Error while sync DB:', e)
    }
  }
}