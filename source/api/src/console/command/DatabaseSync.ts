import typeormConnection from '../../config/typeormConnection'
import { printGreen, printRed } from '../utils'

export default {
  cmd: `db:sync`,
  descrition: `Synchronize database with application schema. Run first db:check to check if alter script is OK`,
  async handler(cmd: string, args: [string]) {
    try {
      const connection = await typeormConnection()

      await connection.synchronize()
      await connection.close()

      printGreen('Schema syncronization finished successfully.')

    } catch (e) {
      printRed('☠️  Error while sync DB:', e)
    }
  }
}