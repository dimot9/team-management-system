import 'module-alias/register'

import { printYellow, printRed } from './utils'
import DatabaseCheck from './command/DatabaseCheck'
import DatabaseSync from './command/DatabaseSync'
import DatabaseReset from './command/DatabaseReset'

/* tslint:disable:no-console */
const commands = [DatabaseCheck, DatabaseSync, DatabaseReset]

const printHelp = () => {
  printYellow('\n Available commands:')
  printYellow(' --------------------')
  commands.forEach((c) => {
    printYellow(
      ` - ${c.cmd}` + ` `.repeat(14 - c.cmd.length) + `${c.descrition}`
    )
  })
}

export default async (cmd: string, args: [string]) => {
  const m = `API CONSOLE`

  printYellow(`\n ${m}`)
  printYellow(` ` + `=`.repeat(m.length))

  if (cmd) {
    // has cmd, try execute command
    printYellow(` 👉  Run command: ${cmd} ${args}`)

    const matchCmd = commands.find((c) => c.cmd === cmd)
    if (matchCmd) {
      console.log()
      await matchCmd.handler(cmd, args)
    } else {
      printRed('\n ☠️  Required command not exists!!!')
      printHelp()
    }
  } else {
    printHelp()
  }
}