import chalk from 'chalk'
/* tslint:disable:no-console */
const printYellow = (msg: string, e?: Error) =>
  console.log(chalk.yellow(`${msg}`), e ? e :'')

const printGreen = (msg: string, e?: Error) =>
  console.log(chalk.green(`${msg}`), e ? e :'')

const printRed = (msg: string, e?: Error) => console.log(chalk.red(`${msg}`), e ? e :'')

export { printYellow, printGreen, printRed }
