import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany, ManyToOne} from 'typeorm'
import { ObjectType, Field } from 'typegql'
import {GraphQLDateTime} from "graphql-iso-date";
import {Member} from "./Member";
import {DiscussionMessage} from "./DiscussionMessage";
import {Season} from "./Season";

@Entity({ name: 'discussion' })
@ObjectType()
export class Discussion extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number

    @ManyToOne(() => Member, { nullable: true, onDelete: "SET NULL" })
    @Field({ type: Member, description: 'Who created the discussion.' })
    author: Member

    @Column({ default: () => "CONCAT('Discussion ', CURRENT_TIMESTAMP)"})
    @Field({ description: 'Title of the discussion.' })
    title: string

    @Column({ default: () => "CURRENT_TIMESTAMP" })
    @Field({ type: GraphQLDateTime, description: 'Beginning of the discussion.' })
    created: Date

    @Column({ default: false })
    @Field({ description: 'If the discussion is not actual anymore.' })
    closed: boolean

    @OneToMany(() => DiscussionMessage, (message: DiscussionMessage) => message.discussion)
    @Field({ type: () => [DiscussionMessage], description: 'All messages of the discussion.' })
    messages: DiscussionMessage[]

    @ManyToOne(() => Season, { nullable: false, onDelete: "CASCADE" })
    @Field({ description: 'The season to which discussion belongs to.' })
    season: Season
}