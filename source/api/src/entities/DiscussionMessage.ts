import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne} from 'typeorm'
import { ObjectType, Field } from 'typegql'
import {GraphQLDateTime} from "graphql-iso-date";
import {Member} from "./Member";
import {Discussion} from "./Discussion";

@Entity({ name: 'discussion_message' })
@ObjectType()
export class DiscussionMessage extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number

    @ManyToOne(() => Member, { nullable: false, onDelete: "CASCADE" })
    @Field({ type: Member, description: 'Who created the message.' })
    author: Member

    @Column({ nullable: false })
    @Field({ description: 'Message itself.' })
    message: string

    @Column({ default: () => "CURRENT_TIMESTAMP" })
    @Field({ type: GraphQLDateTime, description: 'Creation datetime of the message.' })
    created: Date

    @ManyToOne(() => Discussion, (discussion: Discussion) => discussion.messages, { nullable: false, onDelete: "CASCADE" })
    // @Field({ type: () => Discussion, description: 'Message is part of this discussion.' })
    discussion: Discussion
}