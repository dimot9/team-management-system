import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    ManyToMany,
    JoinTable, ManyToOne
} from 'typeorm'
import { ObjectType, Field } from 'typegql'
import {Member} from "./Member";
import {Season} from "./Season";
import {GraphQLDateTime} from "graphql-iso-date";

@Entity({ name: 'duty' })
@ObjectType()
export class Duty extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number

    @Column({ nullable: false })
    @Field({ description: 'Name of the duty.'})
    title: string

    @Column({ nullable: true })
    @Field({ type: GraphQLDateTime, description: 'Start date of the duty.', isNullable: true })
    start: Date

    @Column({ nullable: true })
    @Field({ type: GraphQLDateTime, description: 'End date of the duty.', isNullable: true })
    end: Date

    @Column({ type: "text", nullable: true })
    @Field({ description: 'Description of the duty.'})
    description: string

    @ManyToMany(() => Member)
    @JoinTable()
    @Field({ type: [Member], description: 'List of players who should complete the duty.' })
    players: Member[]

    @ManyToOne(() => Season, { nullable: false })
    @Field({ type: Season, description: 'Season which this duty belongs to.' })
    season: Season
}