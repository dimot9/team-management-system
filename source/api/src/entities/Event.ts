import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    OneToMany, ManyToOne
} from 'typeorm'
import { ObjectType, Field } from 'typegql'
import {Season} from "./Season";
import {GraphQLDateTime} from "graphql-iso-date";
import {EventTypeEnum, GQLEventTypeEnum} from "../enums/EventTypeEnum";
import {EventLineup} from "./EventLineup";

@Entity({ name: 'event' })
@ObjectType()
export class Event extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number

    @Column({ nullable: false })
    @Field({ description: 'Name of the event.' })
    title: string

    @Column({ default: EventTypeEnum.OTHER })
    @Field({ type: GQLEventTypeEnum, description: 'Type of the event.' })
    type: EventTypeEnum

    @Column({ default: true })
    @Field()
    allDay: boolean

    @Column({ nullable: true })
    @Field({ type: GraphQLDateTime, description: 'Start time of the event.', isNullable: true })
    start: Date

    @Column({ nullable: true })
    @Field({ type: GraphQLDateTime, description: 'End time of the event.', isNullable: true })
    end: Date

    // should be earlier then the start of the event...
    @Column({ nullable: true })
    @Field({ type: GraphQLDateTime, description: 'Meeting time for the event.', isNullable: true })
    meetingTime: Date

    @Column({ nullable: true })
    @Field( { isNullable: true, description: 'Address where the event holds a place.' })
    place: string

    @Column({ type: "text", nullable: true })
    @Field( { isNullable: true, description: 'Some additional notes for the event.' })
    notes: string

    /**
     * Custom many to many relation (2x one to many). Relation must include isAbsent property - to know if member can go to the event or not.
     * Lineup - List of members that have to go to the event.
     */
    @OneToMany(() => EventLineup, el => el.event)
    @Field({ type: () => [EventLineup] })
    lineup: EventLineup[]

    @ManyToOne(() => Season, { nullable: false, onDelete: "CASCADE" })
    season: Season
}