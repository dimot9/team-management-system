import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    ManyToOne
} from 'typeorm'
import { ObjectType, Field } from 'typegql'
import {GraphQLDateTime} from "graphql-iso-date";
import {Member} from "./Member";
import { Event as _Event } from "./Event"

@Entity({ name: 'event_comment' })
@ObjectType()
export class EventComment extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number

    @ManyToOne(() => Member, { nullable: false, onDelete: "CASCADE" })
    @Field( {type: Member, description: 'Author of the comment.' })
    author: Member

    @ManyToOne(() => _Event, { nullable: false, onDelete: "CASCADE" })
    @Field({ type: _Event, description: 'Event, which was commented.' })
    event: _Event

    @Column({ nullable: false })
    @Field({ description: 'Comment itself.' })
    message: string

    @Column({ default: () => "CURRENT_TIMESTAMP" })
    @Field({ type: GraphQLDateTime, description: 'Datetime of the comment creation.' })
    created: Date
}