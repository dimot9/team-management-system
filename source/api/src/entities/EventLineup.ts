import {BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryColumn} from "typeorm";
import { Event } from "./Event";
import {Member} from "./Member";
import {Field, ObjectType} from "typegql";

@Entity({ name: 'event_lineup'})
@ObjectType()
export class EventLineup extends BaseEntity {

    @PrimaryColumn()
    eventId: number

    @PrimaryColumn()
    memberId: number

    /**
     * Indicates if member in lineup can go to the event or not
     */
    @Column({ default: false })
    @Field()
    isAbsent: boolean

    @ManyToOne(() => Event, event => event.lineup, { primary: true, onDelete: "CASCADE" })
    @JoinColumn({ name: "eventId"})
    event: Event

    @ManyToOne(() => Member, member => member.assignedEvents, { primary: true, onDelete: "CASCADE" })
    @JoinColumn({ name: "memberId"})
    @Field({type: () => Member})
    member: Member
}