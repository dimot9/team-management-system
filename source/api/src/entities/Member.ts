import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne, OneToMany} from 'typeorm'
import {ObjectType, Field} from 'typegql'
import {User} from "./User";
import {GQLRoleEnum, RoleEnum} from "../enums/RoleEnum";
import {GQLPositionEnum, PositionEnum} from "../enums/PositionEnum";
import {Team} from "./Team";
import {PlayerStatistics} from "./PlayerStatistics";
import {TaskToMember} from "./TaskToMember";
import {EventLineup} from "./EventLineup";

@Entity({ name: 'member' })
@ObjectType()
export class Member extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number

    @ManyToOne(() => User, null, {nullable: true, onDelete: "CASCADE"})
    @Field({ type: User, description: 'User account of a member.' })
    user: User

    @ManyToOne(() => Team, { nullable: false })
    @Field({ type: Team, description: 'Member`s team' })
    team: Team

    @Column({ nullable: false, default: RoleEnum.PLAYER })
    @Field({ type: GQLRoleEnum, description: 'Member role - is he/she coach or player?' })
    role: RoleEnum

    @Column({ nullable: true })
    @Field({ type: GQLPositionEnum, isNullable: true, description: 'Which position is he playing? Null when role is COACH.' })
    position: PositionEnum

    @Column({ nullable: false, default: false })
    @Field({ description: 'Indicates if team member is ill or injured.' })
    isUnable: boolean

    @Column({ nullable: true })
    @Field({ isNullable: true, description: 'Each player has its own or favourite shirt number. Null when role is COACH.'})
    shirtNumber: number

    @Column({ nullable: true })
    @Field({ isNullable: true, description: 'Member can have a nickname which will be displayed instead of name and surname.' })
    nickname: string

    @OneToMany(() => PlayerStatistics, (playerStatistics: PlayerStatistics) => playerStatistics.player)
    playerStatistics: PlayerStatistics[]

    @Column({ nullable: true, unique: true })
    inviteHash: string

    /**
     * Custom many to many relation (2x one to many). Relation must include completed property to distinguish which player completed the task.
     */
    @OneToMany(() => TaskToMember, ttm => ttm.member)
    @Field({ type: () => [TaskToMember]})
    assignedTasks: TaskToMember[]

    /**
     * Custom many to many relation (2x one to many). Relation must include isAbsent property - to know if member can go to the event or not.
     * AssignedEvents - List of events in which is member in lineup
     */
    @OneToMany(() => EventLineup, ttm => ttm.member)
    @Field({ type: () => [EventLineup]})
    assignedEvents: EventLineup[]
}
