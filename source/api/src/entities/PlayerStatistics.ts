import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne} from 'typeorm'
import { ObjectType, Field } from 'typegql'
import {Season} from "./Season";
import {Member} from "./Member";

@Entity({ name: 'player_statistics' })
@ObjectType()
export class PlayerStatistics extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number

    @ManyToOne(() => Season, { nullable: false, onDelete: "CASCADE" })
    @Field({ description: 'The season to which statistics belong to.' })
    season: Season

    @ManyToOne(() => Member, (member: Member) => member.playerStatistics, { nullable: false, onDelete: "CASCADE" })
    player: Member

    @Column({ default: 0 })
    @Field({ description: 'Amount of goals player scored through the season.' })
    goalsScored: number

    @Column({ default: 0 })
    @Field({ description: 'Amount of minutes player played through the season.' })
    minutesPlayed: number

    @Column({ default: 0 })
    @Field({ description: 'Amount of assists player contributed through the season.' })
    assists: number
}