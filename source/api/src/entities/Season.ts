import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne} from 'typeorm'
import { ObjectType, Field } from 'typegql'
import { GraphQLDateTime } from 'graphql-iso-date'
import {Team} from "./Team";

@Entity({ name: 'season' })
@ObjectType()
export class Season extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number

    @ManyToOne(() => Team, (team: Team) => team.seasons, { onDelete: "CASCADE" })
    team: Team

    @Column({ nullable: false })
    @Field({ description: 'Name of the season.'})
    name: string

    @Column({ nullable: true })
    @Field({ type: GraphQLDateTime, description: 'Start date of the season.', isNullable: true })
    start: Date

    @Column({ nullable: true })
    @Field({ type: GraphQLDateTime, description: 'End date of the season.', isNullable: true })
    end: Date
}