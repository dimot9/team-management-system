import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    ManyToOne, OneToMany
} from 'typeorm'
import { ObjectType, Field } from 'typegql'
import {Season} from "./Season";
import {TaskToMember} from "./TaskToMember";

@Entity({ name: 'task' })
@ObjectType()
export class Task extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number

    @Column({ nullable: false })
    @Field({ description: 'Name of the task.'})
    name: string

    @Column({ type: "text", nullable: true })
    @Field({ description: 'Description of the task.'})
    description: string

    @ManyToOne(() => Season, { nullable: false, onDelete: "CASCADE" })
    @Field({ type: Season, description: 'Season which this task belongs to.' })
    season: Season

    /**
     * Custom many to many relation (2x one to many). Relation must include completed property to distinguish which player completed the task.
     */
    @OneToMany(() => TaskToMember, t => t.task)
    @Field({ type: () => [TaskToMember] })
    assignedMembers: TaskToMember[]
}