import {BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryColumn, PrimaryGeneratedColumn} from "typeorm";
import {Task} from "./Task";
import {Member} from "./Member";
import {Field, ObjectType} from "typegql";

@Entity({ name: 'task_to_member'})
@ObjectType()
export class TaskToMember extends BaseEntity {

    @PrimaryColumn()
    taskId: number

    @PrimaryColumn()
    memberId: number

    @Column({ default: false })
    @Field()
    completed: boolean

    @ManyToOne(() => Task, task => task.assignedMembers, { primary: true, onDelete: "CASCADE" })
    @JoinColumn({ name: "taskId"})
    task: Task

    @ManyToOne(() => Member, member => member.assignedTasks, { primary: true, onDelete: "CASCADE" })
    @JoinColumn({ name: "memberId"})
    @Field({type: () => Member})
    member: Member
}