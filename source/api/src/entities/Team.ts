import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany, OneToOne, JoinColumn} from 'typeorm'
import { ObjectType, Field } from 'typegql'
import {Member} from "./Member";
import {Season} from "./Season";
import {TeamStatistics} from "./TeamStatistics";

@Entity({ name: 'team' })
@ObjectType()
export class Team extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number

    @Column({ nullable: false })
    @Field()
    name: string

    @OneToOne(type => Season, { nullable: false })
    @JoinColumn()
    @Field({ type: Season })
    currentSeason: Season

    @OneToMany(() => Season, (season: Season) => season.team, { cascade: true, onDelete: "CASCADE"})
    @Field({ type: [Season], description: 'All seasons of the team. '})
    seasons: Season[]

    @OneToMany(() => TeamStatistics, (teamStatistics: TeamStatistics) => teamStatistics.team)
    teamStatistics: TeamStatistics[]
}