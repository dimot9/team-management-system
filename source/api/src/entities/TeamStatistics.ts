import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne, OneToOne, JoinColumn} from 'typeorm'
import { ObjectType, Field } from 'typegql'
import {Season} from "./Season";
import {Team} from "./Team";

@Entity({ name: 'team_statistics' })
@ObjectType()
export class TeamStatistics extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number

    @OneToOne(() => Season, { nullable: false })
    @JoinColumn()
    @Field({ description: 'The season to which statistics belong to.' })
    season: Season

    @Column({ default: 0 })
    @Field({ isNullable: true, description: 'Amount of goals team scored through the season.' })
    goalsScored: number

    @Column({ default: 0 })
    @Field({ isNullable: true, description: 'Amount of goals team scored through the season.' })
    goalsReceived: number

    @ManyToOne(() => Team, (team: Team) => team.teamStatistics)
    @Field({ type: Team })
    team: Team
}