import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from 'typeorm'
import { ObjectType, Field } from 'typegql'

@Entity({ name: 'user' })
@ObjectType()
export class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number

    @Column({ unique: true, nullable: true })
    @Field()
    email: string

    @Column()
    password: string

    @Column()
    salt: string

    @Column()
    @Field()
    name: string

    @Column()
    @Field()
    surname: string

    @Field()
    fullname(): string {
        return `${this.name} ${this.surname}`
    }

    @Column({ nullable: true, unique: true })
    forgotPasswordToken: string

    @Column({ nullable: true })
    forgotPasswordExpiration: Date
}

export interface PasswordData {
    hash: string
    salt: string
}