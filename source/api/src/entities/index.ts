import { User } from './User'
import { Member } from './Member'
import { Team } from './Team'
import { Season } from "./Season";
import { PlayerStatistics } from "./PlayerStatistics";
import { TeamStatistics } from  "./TeamStatistics"
import { Event} from "./Event";
import { EventComment} from "./EventComment";
import { Discussion } from "./Discussion";
import { DiscussionMessage } from "./DiscussionMessage";
import { Task } from "./Task";
import { Duty } from "./Duty";
import {TaskToMember} from "./TaskToMember";
import { EventLineup } from "./EventLineup";

export default [ User, Member, Team, Season, PlayerStatistics, TeamStatistics, Event, EventComment, Discussion, DiscussionMessage, Task, Duty, TaskToMember, EventLineup ]