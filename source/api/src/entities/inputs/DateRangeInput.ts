import {InputField, InputObjectType} from "typegql";
import {GraphQLDateTime} from "graphql-iso-date";

@InputObjectType()
export class DateRangeInput {

    @InputField({type: GraphQLDateTime, isNullable: false })
    start: Date

    @InputField({type: GraphQLDateTime, isNullable: true })
    end: Date
}