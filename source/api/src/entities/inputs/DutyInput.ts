import {ID, InputField, InputObjectType} from "typegql";
import {GraphQLDateTime} from "graphql-iso-date";

@InputObjectType()
export class DutyInput {

    @InputField({ isNullable: true })
    title: string

    @InputField({ type: GraphQLDateTime, isNullable: true })
    start: Date

    @InputField({ type: GraphQLDateTime, isNullable: true })
    end: Date

    @InputField({ isNullable: true })
    description: string

    @InputField({ type: [ID], isNullable: true })
    players: number[]
}