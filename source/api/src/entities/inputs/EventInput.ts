import {ID, InputField, InputObjectType} from "typegql";
import {EventTypeEnum, GQLEventTypeEnum} from "../../enums/EventTypeEnum";
import {GraphQLDateTime} from "graphql-iso-date";

@InputObjectType()
export class EventInput {

    @InputField({ isNullable: true })
    title: string

    @InputField({ type: GQLEventTypeEnum, isNullable: true })
    type: EventTypeEnum

    @InputField({ isNullable: true })
    allDay: boolean

    @InputField({ type: GraphQLDateTime, isNullable: true })
    start: Date

    @InputField({ type: GraphQLDateTime, isNullable: true })
    end: Date

    @InputField({ type: GraphQLDateTime, isNullable: true })
    meetingTime: Date

    @InputField({ isNullable: true })
    notes: string

    @InputField({ isNullable: true })
    place: string

    @InputField({ type: [ID], isNullable: true })
    lineup: number[]
}