import {InputField, InputObjectType, Int} from "typegql";
import {GQLRoleEnum, RoleEnum} from "../../enums/RoleEnum";
import {GQLPositionEnum, PositionEnum} from "../../enums/PositionEnum";

@InputObjectType()
export class MemberInput {

    @InputField({ type: GQLRoleEnum, isNullable: true })
    role: RoleEnum

    @InputField({ type: GQLPositionEnum, isNullable: true })
    position: PositionEnum

    @InputField({ isNullable: true })
    isUnable: boolean

    @InputField({ isNullable: true })
    nickname: string

    @InputField({ type: Int, isNullable: true })
    shirtNumber: number
}