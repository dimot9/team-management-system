import {ID, InputField, InputObjectType, Int} from "typegql";

@InputObjectType()
export class PlayerStatisticsInput {

    @InputField({type: ID, isNullable: true })
    id: number

    @InputField({type: Int, isNullable: true })
    minutesPlayed: number

    @InputField({type: Int, isNullable: true })
    goalsScored: number

    @InputField({type: Int, isNullable: true })
    assists: number
}