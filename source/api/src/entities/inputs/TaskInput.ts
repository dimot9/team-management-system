import {ID, InputField, InputObjectType} from "typegql";

@InputObjectType()
export class TaskInput {

    @InputField({ isNullable: true })
    name: string

    @InputField({ isNullable: true })
    description: string

    @InputField({ isNullable: true })
    completed: boolean

    @InputField({ type: [ID], isNullable: true })
    members: number[]
}