import {InputField, InputObjectType} from "typegql";

@InputObjectType()
export class UserInput {

    @InputField({ isNullable: true })
    name: string

    @InputField({ isNullable: true })
    surname: string

    @InputField({ isNullable: true })
    email: string
}