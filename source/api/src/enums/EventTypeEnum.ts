import { GraphQLEnumType } from 'graphql'

export const GQLEventTypeEnum = new GraphQLEnumType({
         name: 'EventTypeEnum',
         values: {
           TRAINING: {
             value: 'TRAINING'
           },
           MATCH: {
             value: 'MATCH'
           },
           TOURNAMENT: {
             value: 'TOURNAMENT'
           },
           OTHER: {
             value: 'OTHER'
           }
         }
       })

export enum EventTypeEnum {
  TRAINING = 'TRAINING',
  MATCH = 'MATCH',
  TOURNAMENT = 'TOURNAMENT',
  OTHER = 'OTHER'
}