import { GraphQLEnumType } from 'graphql'

export const GQLPositionEnum = new GraphQLEnumType({
         name: 'PositionEnum',
         values: {
            DEFENDER: {
             value: 'DEFENDER'
           },
            MIDFIELDER: {
             value: 'MIDFIELDER'
           },
            FORWARD: {
             value: 'FORWARD'
           }
         }
       })

export enum PositionEnum {
  DEFENDER = 'DEFENDER',
  MIDFIELDER = 'MIDFIELDER',
  FORWARD = 'FORWARD'
}