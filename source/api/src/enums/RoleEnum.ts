import { GraphQLEnumType } from 'graphql'

export const GQLRoleEnum = new GraphQLEnumType({
         name: 'RoleEnum',
         values: {
           COACH: {
             value: 'COACH'
           },
           PLAYER: {
             value: 'PLAYER'
           }
         }
       })

export enum RoleEnum {
  COACH = 'COACH',
  PLAYER = 'PLAYER'
}