import {ErrorEnum, getErrorByName} from "./index";

export class TokenExpiredError extends Error {

    code: number

    key: string

    message: string

    constructor() {
        super();

        // Maintains proper stack trace for where our error was thrown (only available on V8)
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, TokenExpiredError)
        }

        const {code, key, message } = getErrorByName(ErrorEnum.TokenExpired)

        this.code = code
        this.key = key
        this.message = message
    }
}