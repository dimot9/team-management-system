export enum ErrorEnum {
    Unauthorized = 'Unauthorized',
    UserNotFound = 'UserNotFound',
    UserExists = 'UserExists',
    InvalidPassword = 'InvalidPassword',
    InternalServerError = 'InternalServerError',
    NotFound = 'NotFound',
    TokenExpired = 'TokenExpired',
    ForgotPasswordTokenInvalid = 'ForgotPasswordTokenInvalid',
    MemberAlreadyExists = 'MemberAlreadyExists',
    InviteInvalid = 'InviteInvalid',
    Forbidden = 'Forbidden',
    BadRequest = 'BadRequest'
}

const Errors = {
    Unauthorized: {
        message: 'Authentication is needed.',
        key: 'notAllowed',
        code: 401
    },
    TokenExpired: {
        message: 'Your token expired, sign in again.',
        key: 'tokenExpired',
        code: 401
    },
    UserNotFound: {
        message: 'User was not found.',
        key: 'userNotFound',
        code: 404
    },
    UserExists: {
        message: 'User with this email already exists.',
        key: 'userExists',
        code: 303
    },
    InvalidPassword: {
        message: 'User entered wrong password.',
        key: 'invalidPassword',
        code: 401
    },
    InternalServerError: {
        message: 'Something went wrong on our side. Try again later.',
        key: 'internalServerError',
        code: 500
    },
    NotFound: {
        message: 'Resource not found.',
        key: 'notFound',
        code: 404
    },
    ForgotPasswordTokenInvalid: {
        message: 'Password reset token is invalid or expired.',
        key: 'forgotPasswordTokenInvalid',
        code: 400
    },
    MemberAlreadyExists: {
        message: 'Member with this email already exists.',
        key: 'memberAlreadyExists',
        code: 409
    },
    InviteInvalid: {
        message: 'Invitation link is invalid or expired.',
        key: 'inviteInvalid',
        code: 400
    },
    Forbidden: {
        message: 'You have no access to this resource.',
        key: 'forbidden',
        code: 403
    },
    BadRequest: {
        message: 'Your request data are invalid, nonsensical or exceeds server`s limitation.',
        key: 'badRequest',
        code: 400
    }
}

export const getErrorByName = (errorName: ErrorEnum) => Errors[errorName]