import 'reflect-metadata'
import {GraphQLServer} from 'graphql-yoga'
import schema from './schema'
import loader from './loaders'
import Logger from "./loaders/logger";
import { getErrorByName } from "./errors";
import {GraphQLError} from "graphql";
import TokenMiddleware from "./middlewares/TokenMiddleware";
import cookieParser = require('cookie-parser');
import Config from "./loaders/config";

async function startServer(): Promise<void> {
    process.title = 'tms'
    // create server
    const server = new GraphQLServer({
        schema,
        context: (req: Request): any => ({
            ...req,
        })
    });

    server.express.use(cookieParser())

    // load all necessary services
    await loader()
    // middlewares
    server.express.use(TokenMiddleware)

    // start server
    server.start(
        {
            port: Config.port,
            playground: '/playground',
            formatError: (err: any) => {
                const error = getErrorByName(err.message)
                if (!error) {
                    Logger.error(err)
                    return { message: err.message, code: (err instanceof GraphQLError) ? 400 : 500 }
                }
                Logger.error(error.message)
                return { message: error.message, code: error.code, key: error.key }
            }
        },
        () => {
            Logger.info(`✌️ Server is running on ${Config.virtualHost}:${Config.port}`);
            Logger.info(
                `On development mode is posible visit sandbox GraphQL API on http://${Config.virtualHost}/playground`
            )
        }
    )
}

startServer();

