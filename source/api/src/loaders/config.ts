import * as dotenv from 'dotenv'

dotenv.config()
let path
switch (process.env.NODE_ENV) {
    case "test":
        path = `${__dirname}/../../../../.env.test`
        break;
    case "dev":
        path = `${__dirname}/../../../../.env`
        break;
    default:
        path = `${__dirname}/../../../../.env`
        break;
}
const { parsed } = dotenv.config(({ path }))

const Config = {
    /**
     * Your favorite port
     */
    port: '4000',
    virtualHost: 'localhost',

    db: {
        host: parsed.DB__HOST || '',
        port: parseInt(parsed.DB__PORT || '', 10),
        user: parsed.DOCKER__DB__USER|| '',
        password: parsed.DOCKER__DB__PASSWORD || '',
        database: parsed.DOCKER__DB__DATABASE || ''
    },

    smtp: {
        host: parsed.SMTP_HOST || "smtp.mailtrap.io",
        port: parseInt(parsed.SMTP_PORT, 10) || 2525,
        auth: {
            user: parsed.SMTP_USER || "6a8d2bff1b02a3",
            pass: parsed.SMTP_PASSWORD ||"ddd3afbd3a2b94"
        }
    },

    token: {
        secret: parsed.JWT_SECRET || 'tms',
        expiration: 7200 // Token expiration in sec
    },

    /**
     * Used by winston logger
     */
    logs: {
        level: parsed.LOG_LEVEL || 'silly'
    }
}

export default Config