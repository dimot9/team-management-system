import Logger from "./logger";
import entities from '../entities'
import Container from 'typedi'
import {createConnection, useContainer, Connection, ConnectionOptions} from 'typeorm'
import { sleep } from '../utils/system'
import Config from "./config";

function getConnectionOptions(): ConnectionOptions {
    let options: ConnectionOptions = {
        name: "default",
        type: 'mariadb',
        host: Config.db.host,
        port: Config.db.port,
        username: Config.db.user,
        password: Config.db.password,
        database: Config.db.database,
        entities,
        logging: ['error', 'info', 'warn', 'query'],
        synchronize: true
    }

    if (process.env.NODE_ENV === 'test') {
        options = {
            name: "default",
            type: 'mariadb',
            host: Config.db.host,
            port: Config.db.port,
            username: Config.db.user,
            password: Config.db.password,
            database: Config.db.database,
            entities,
            logging: false,
            synchronize: true,
            dropSchema: true
        }
    }

    return options
}

async function tryConnect(): Promise<Connection> {
    return await createConnection(getConnectionOptions())
}

export default async (): Promise<void> => {
    // typeORM use typeDI container
    useContainer(Container)
    let attempt = 5
    let connection: any = null
    Logger.info('⏳ Waiting for DB...')

    // try connect multiple times - avoid freeze while connecting
    while (connection == null && attempt > 0) {
        try {
            connection = await tryConnect()
            Logger.info('💾Connection has been established successfully.')
        } catch (e) {
            Logger.error(e.message)
            Logger.error(
                '☠️ Unable to connect to the database. Trying again...'
            )
            attempt--
            if (attempt <= 0) {
                Logger.error(
                    `☠️ Unable to connect to the database after 5 attempts. Check your configuration.`
                )
            }
            await sleep(1000)
        }
    }
}
