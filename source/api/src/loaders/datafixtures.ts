import LoggerInstance from './logger'
import Container from 'typedi'
import { DataFixturesService } from '../services/DataFixturesService'

export default async (): Promise<void> => {
  try {
    LoggerInstance.info('Data fixtures loaded successfully and fuse search was updated.')
  } catch (e) {
    LoggerInstance.error('☠️ Unable to load data fixtures to database:', e)
  }
}
