import Logger from "./logger";
import createDatabase from './database'

export default async (): Promise<void> => {
    Logger.info("Loading main server services...")

    await createDatabase()
}