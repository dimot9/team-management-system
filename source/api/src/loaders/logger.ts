import * as winston from 'winston'
import Config from "./config";

const { transports, format } = winston

const print = format.printf((info) => {
    const log = `${new Date().toLocaleString()}: ${info.level}: ${info.message}`

    return info.stack ? `${log}\n Stack trace: \n${info.stack}` : log
})

const Logger = winston.createLogger({
    level: Config.logs.level,
    format: format.combine(format.colorize(), format.errors({ stack: true }), print),
    transports: [
        new transports.Console(),
        new transports.File({ filename: 'logs/error.log', level: 'error' })
    ]
})

export default Logger