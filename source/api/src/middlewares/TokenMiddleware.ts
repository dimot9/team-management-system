import * as JWT from 'jsonwebtoken'
import {TokenExpiredError} from 'jsonwebtoken'
import { TokenExpiredError as TokenError } from '../errors/TokenExpiredError'
import {RoleEnum} from "../enums/RoleEnum";
import {Container} from "typedi";
import {MemberRepository} from "../repositories/MemberRepository";
import Config from "../loaders/config";

export default function(req: any, res: any, next: any) {
    let token = req.headers['x-access-token'] || req.headers.authorization; // Express headers are auto converted to lowercase
    let executeNext = true
    if (token) {
        if (token.startsWith('Bearer ')) {
            // Remove Bearer from string
            token = token.slice(7, token.length);
        }

        JWT.verify(
            token,
            Config.token.secret,
            (error: any, user: any) => {
                if (error) {
                    if (error instanceof TokenExpiredError) {
                        executeNext = false
                        res.status(401).send(new TokenError())
                    }
                } else if (user) {
                    req.user = Object.assign({
                        id: null,
                        email: null
                    }, user)
                }
            }
        )
    }

    /**
     * Check if incoming
     * @param memberRoles
     */
    req.isAllowed = async (memberRoles: RoleEnum[]) => {
        const memberId = req.headers['x-tms-member'] || null
        if (req.user && memberId) {
            const memberRepository: MemberRepository = Container.get(MemberRepository)
            const _member = await memberRepository.findOne({
                where: {
                    id: memberId,
                    user: req.user.id
                }
            })

            if (_member) {
                req.member = _member
                return memberRoles.includes(_member.role)
            }
            return false
        }
        return false
    }

    if(executeNext) {
        next()
    }
}