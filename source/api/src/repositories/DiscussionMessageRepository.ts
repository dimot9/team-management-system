import {EntityRepository, getCustomRepository, Repository} from "typeorm";
import {Service} from "typedi";
import {DiscussionMessage} from "../entities/DiscussionMessage";

@EntityRepository(DiscussionMessage)
@Service({ factory: () => getCustomRepository(DiscussionMessageRepository)})
export class DiscussionMessageRepository extends Repository<DiscussionMessage> {

}