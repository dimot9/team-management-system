import {EntityRepository, getCustomRepository, Repository} from "typeorm";
import {Service} from "typedi";
import {Discussion} from "../entities/Discussion";

@EntityRepository(Discussion)
@Service({ factory: () => getCustomRepository(DiscussionRepository)})
export class DiscussionRepository extends Repository<Discussion> {

    /**
     * Find all discussions by season. Season belongs to ONE team. Join many-to-one relation table <author: Member>
     * @param seasonId
     */
    findBySeason(seasonId: number, limit?: number) {
        return this.find({
            relations: ['author', 'author.user'],
            where: {
                season: seasonId
            },
            order: {
                created: 'DESC'
            },
            take: limit ?? null
        })
    }
}