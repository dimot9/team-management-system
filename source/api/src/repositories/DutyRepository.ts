import {
    Between,
    EntityRepository,
    FindManyOptions,
    getCustomRepository,
    LessThanOrEqual,
    MoreThanOrEqual,
    Repository
} from "typeorm";
import {Service} from "typedi";
import {Duty} from "../entities/Duty";
import {DateRangeInput} from "../entities/inputs/DateRangeInput";
import {Event as _Event} from "../entities/Event";

@EntityRepository(Duty)
@Service({ factory: () => getCustomRepository(DutyRepository)})
export class DutyRepository extends Repository<Duty> {

    /**
     * Find all duties by season. Season belongs to ONE team.
     * @param seasonId
     * @param range
     * @param limit How many duties should be returned
     */
    findBySeason(seasonId: number, range?: DateRangeInput, limit?: number) {
        const options: FindManyOptions<Duty> = {
            relations: ["players", "players.user"],
            where: {
                season: seasonId
            }
        }

        if (range) {
            options.where = [
                {
                    season: seasonId,
                    start: Between(range.start, range.end),
                },
                {
                    season: seasonId,
                    end: Between(range.start, range.end)
                }
            ]
        }

        if (limit) {
            options.take = limit
        }

        return this.find(options)
    }

    findTodayDuties(seasonId: number) {
        return this.find({
            relations: ["players", "players.user"],
            where: {
                season: seasonId,
                start: LessThanOrEqual(new Date()),
                end: MoreThanOrEqual(new Date())
            }
        })
    }
}