import {EntityRepository, getCustomRepository, Repository} from "typeorm";
import {Service} from "typedi";
import {EventComment} from "../entities/EventComment";

@EntityRepository(EventComment)
@Service({ factory: () => getCustomRepository(EventCommentRepository)})
export class EventCommentRepository extends Repository<EventComment> {
}