import {EntityRepository, getCustomRepository, Repository} from "typeorm";
import {Service} from "typedi";
import {EventLineup} from "../entities/EventLineup";

@EntityRepository(EventLineup)
@Service({ factory: () => getCustomRepository(EventLineupRepository)})
export class EventLineupRepository extends Repository<EventLineup> {}