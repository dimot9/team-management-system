import {Between, EntityRepository, FindManyOptions, getCustomRepository, MoreThanOrEqual, Repository} from "typeorm";
import {Service} from "typedi";
import {Event, Event as _Event} from "../entities/Event"
import {DateRangeInput} from "../entities/inputs/DateRangeInput";
import dayjs from "dayjs";

@EntityRepository(_Event)
@Service({ factory: () => getCustomRepository(EventRepository)})
export class EventRepository extends Repository<_Event> {

    /**
     * Find all events by season. Season belongs to ONE team.
     * @param seasonId
     * @param range
     * @param limit How many events should be returned
     */
    findBySeason(seasonId: number, range?: DateRangeInput, limit?: number) {
        const options: FindManyOptions<_Event> = {
            relations: ["season", "season.team"],
            where: {
                season: seasonId
            },
            order: {
                start: 'ASC'
            }
        }

        if (range) {
            if (range.end) {
                options.where = [
                    {
                        season: seasonId,
                        start: Between(range.start, range.end),
                    },
                    {
                        season: seasonId,
                        end: Between(range.start, range.end)
                    }
                ]
            } else {
                options.where = {
                    season: seasonId,
                    start: MoreThanOrEqual(range.start),
                }
            }

        }

        if (limit) {
            options.take = limit
        }

        return this.find(options)
    }
}