import {EntityRepository, getCustomRepository, Repository} from "typeorm";
import {Service} from "typedi";
import {Member} from "../entities/Member";
import {RoleEnum} from "../enums/RoleEnum";

@EntityRepository(Member)
@Service({ factory: () => getCustomRepository(MemberRepository)})
export class MemberRepository extends Repository<Member> {


    /**
     * Finds all members. If member role is provided then filters by member role
     * @param memberRole typically COACH or PLAYER
     */
    findAllOrByRole(memberRole?: RoleEnum): Promise<Member[]> {
        return this.find({
            relations: ["user"],
            where: {
                role: memberRole
            }
        })
    }

    /**
     * Finds all members, which are created from user with @param userId
     * @param userId
     */
    findByUserId(userId: number): Promise<Member[]> {
        return this.find({
            relations: ["team", "team.currentSeason"],
            where: {
                user: userId
            }
        })
    }

    /**
     * Finds all members of the team
     * @param teamId
     */
    findByTeam(teamId: number): Promise<Member[]> {
        return this.find({
            relations: ['user'],
            where: {
                team: teamId
            },
            order: {
                role: 'DESC'
            }
        })
    }

    /**
     * Finds member of the team linked to certain user
     * @param teamId
     * @param userEmail
     */
    findByTeamAndUserEmail(teamId: number, userEmail: string): Promise<Member> {
        return this
            .createQueryBuilder("member")
            .innerJoin('member.user', 'user')
            .where("member.team = :team", { team: teamId })
            .andWhere('user.email = :email', { email: userEmail })
            .getOne()
    }

    /**
     * Find member with an unique invite hash. When coach creates a new member without an account.
     * @param inviteHash
     */
    findByInviteHash(inviteHash: string): Promise<Member> {
        return this.findOne({
            where: {
                inviteHash
            }
        })
    }
}