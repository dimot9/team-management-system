import {EntityRepository, getCustomRepository, Repository} from "typeorm";
import {Service} from "typedi";
import {PlayerStatistics} from "../entities/PlayerStatistics";

@EntityRepository(PlayerStatistics)
@Service({ factory: () => getCustomRepository(PlayerStatisticsRepository)})
export class PlayerStatisticsRepository extends Repository<PlayerStatistics> {

}