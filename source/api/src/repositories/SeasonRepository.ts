import {EntityRepository, getCustomRepository, Repository} from "typeorm";
import {Service} from "typedi";
import {Season} from "../entities/Season";

@EntityRepository(Season)
@Service({ factory: () => getCustomRepository(SeasonRepository)})
export class SeasonRepository extends Repository<Season> {

}