import {EntityRepository, getCustomRepository, Repository, SelectQueryBuilder} from "typeorm";
import {Service} from "typedi";
import {Task} from "../entities/Task";

@EntityRepository(Task)
@Service({ factory: () => getCustomRepository(TaskRepository)})
export class TaskRepository extends Repository<Task> {

    /**
     * Find all tasks by season. Season belongs to ONE team. Join many-to-many relation table, to get assigned members and completed state.
     * @param seasonId
     */
    findBySeason(seasonId: number) {
        return this.find({
            relations: ['assignedMembers', 'assignedMembers.member', 'assignedMembers.member.user'],
            where: {
                season: seasonId
            }
        })
    }

    /**
     * Find all tasks to which provided member is assigned
     * @param seasonId
     * @param memberId Member who is assigned to this tasks
     */
    findBySeasonAndMember(seasonId: number, memberId: number) {
        return this.find({
            join: {
                alias: 'task',
                innerJoinAndSelect: {
                    assignedMembers: 'task.assignedMembers',
                    assignedMember: 'assignedMembers.member'
                }
            },
            where: (qb: SelectQueryBuilder<Task>) => {
                qb.where({
                    season: seasonId
                }).andWhere('assignedMember.id = :memberId', { memberId })
            }
        })
    }
}