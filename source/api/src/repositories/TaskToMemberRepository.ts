import {EntityRepository, getCustomRepository, Repository} from "typeorm";
import {Service} from "typedi";
import {TaskToMember} from "../entities/TaskToMember";

@EntityRepository(TaskToMember)
@Service({ factory: () => getCustomRepository(TaskToMemberRepository)})
export class TaskToMemberRepository extends Repository<TaskToMember> {}