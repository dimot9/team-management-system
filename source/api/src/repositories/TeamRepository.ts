import {EntityRepository, getCustomRepository, Repository} from "typeorm";
import {Service} from "typedi";
import {Team} from "../entities/Team";

@EntityRepository(Team)
@Service({ factory: () => getCustomRepository(TeamRepository)})
export class TeamRepository extends Repository<Team> {

}