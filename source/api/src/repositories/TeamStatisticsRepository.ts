import {EntityRepository, getCustomRepository, Repository} from "typeorm";
import {Service} from "typedi";
import {TeamStatistics} from "../entities/TeamStatistics";

@EntityRepository(TeamStatistics)
@Service({ factory: () => getCustomRepository(TeamStatisticsRepository)})
export class TeamStatisticsRepository extends Repository<TeamStatistics> {

}