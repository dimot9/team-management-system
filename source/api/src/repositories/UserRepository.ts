import {User} from "../entities/User";
import {EntityRepository, getCustomRepository, MoreThan, Repository} from "typeorm";
import {Service} from "typedi";

@EntityRepository(User)
@Service({ factory: () => getCustomRepository(UserRepository)})
export class UserRepository extends Repository<User> {

    findByEmail(email: string) {
        return this.findOne({ where: { email } })
    }

    findUserByToken(token: string) {
        return this.findOne({ where: { forgotPasswordToken: token, forgotPasswordExpiration: MoreThan(new Date()) } })
    }
}