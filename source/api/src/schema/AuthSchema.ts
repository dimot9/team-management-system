import {Arg, compileSchema, Context, ID, Mutation, Query, SchemaRoot} from 'typegql'
import { Context as ContextType } from 'graphql-yoga/dist/types'
import {Container} from "typedi";
import { AuthService } from "../services/AuthService";
import {User} from "../entities/User";
import {ErrorEnum} from "../errors";
import {UserRepository} from "../repositories/UserRepository";
import {AuthType} from "./types/AuthType";
import {mailService, MailServiceInterface} from "../services/mailService";

@SchemaRoot()
class AuthSchema {

    private authService: AuthService = Container.get(AuthService)
    private userRepository: UserRepository = Container.get(UserRepository)

    /**
     * Gets current user
     * @param ctx
     */
    @Query({ type: User })
    me(
        @Context ctx: ContextType
    ): Promise<User | undefined> {

        // you are not logged in
        if (!ctx.request.user) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        // find self
        return this.userRepository.findOne(ctx.request.user.id)
    }

    /**
     * Registers new user
     * @param name
     * @param surname
     * @param email
     * @param password
     * @param inviteHash
     */
    @Mutation({ type: String })
    async register(
        name: string,
        surname: string,
        email: string,
        password: string,
        @Arg({ isNullable: true })
        inviteHash?: string,
    ): Promise<string> {
        // TODO validation
        await this.authService.registerUser(email, password, name, surname, inviteHash)
        return 'User successfully registered';
    }

    /**
     * Logs in the user
     * @param ctx
     * @param email
     * @param password
     */
    @Mutation({ type: AuthType })
    async login(
        @Context ctx: ContextType,
        email: string,
        password: string
    ): Promise<AuthType > {
        // TODO validation
        return await this.authService.login(email, password)
    }

    @Mutation({ type: String })
    async forgotPassword(
        email: string
    ): Promise<string> {
        await this.authService.forgotPassword(email)
        return "Success! We have send you recovery link."
    }

    @Mutation({ type: String })
    async recoveryPassword(
        password: string,
        token: string
    ): Promise<string> {
        await this.authService.recoveryPassword(token, password)
        return "Success! Your password was changed."
    }
}

export default compileSchema({ roots: [AuthSchema] })