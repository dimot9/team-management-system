import {Arg, compileSchema, Context, ID, Int, Mutation, Query, SchemaRoot} from "typegql";
import {Container} from "typedi";
import {Context as ContextType} from "graphql-yoga/dist/types";
import {SeasonRepository} from "../repositories/SeasonRepository";
import {ErrorEnum} from "../errors";
import {RoleEnum} from "../enums/RoleEnum";
import {DiscussionRepository} from "../repositories/DiscussionRepository";
import {Discussion} from "../entities/Discussion";
import Logger from "../loaders/logger";
import {DiscussionMessage} from "../entities/DiscussionMessage";
import {DiscussionMessageRepository} from "../repositories/DiscussionMessageRepository";
import {QueryDeepPartialEntity} from "typeorm/query-builder/QueryPartialEntity";

@SchemaRoot()
class DiscussionSchema {

    private discussionRepository: DiscussionRepository = Container.get(DiscussionRepository)
    private discussionMessageRepository: DiscussionMessageRepository = Container.get(DiscussionMessageRepository)
    private seasonRepository: SeasonRepository = Container.get(SeasonRepository)

    @Query({ type: [Discussion] })
    async discussions(
        @Context ctx: ContextType,
        @Arg({ type: ID, description: "Season to which discussions belong to." })
        seasonId: number,
        @Arg({ type: Int, isNullable: true , description: 'Limit result discussions count.'})
        limit: number
    ): Promise<Discussion[]> {

        if (!await ctx.request.isAllowed([RoleEnum.COACH, RoleEnum.PLAYER])) {
            throw new Error(ErrorEnum.Unauthorized)
        }
        const _season = await this.seasonRepository.findOne(seasonId)
        if (!_season) {
            throw new Error(ErrorEnum.NotFound)
        }

        try {
            return await this.discussionRepository.findBySeason(seasonId, limit)
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Query({ type: Discussion })
    async discussion(
        @Context ctx: ContextType,
        @Arg({ type: ID })
        discussionId: number
    ): Promise<Discussion> {

        if (!await ctx.request.isAllowed([RoleEnum.COACH, RoleEnum.PLAYER])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        try {
            return await this.discussionRepository.findOne(discussionId, {
                relations: ['author', 'author.user', 'messages', 'messages.author', 'messages.author.user']
            })
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Mutation({ type: Discussion })
    async discussionCreate(
        @Context ctx: ContextType,
        discussionTitle: string,
        @Arg({ type: ID, description: "Season to which task belongs to." })
        seasonId: number
    ): Promise<Discussion> {

        if (!await ctx.request.isAllowed([RoleEnum.COACH, RoleEnum.PLAYER])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        const _season = await this.seasonRepository.findOne(seasonId)
        if (!_season) {
            throw new Error(ErrorEnum.NotFound)
        }

        try {
            const _discussion = new Discussion()
            _discussion.title = discussionTitle
            _discussion.author = ctx.request.member
            _discussion.season = _season
            _discussion.created = new Date()
            return await this.discussionRepository.save(_discussion)
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Mutation( { type: String })
    async discussionUpdate(
        @Context ctx: ContextType,
        @Arg({ type: ID! })
        discussionId: number,
        @Arg({ isNullable: true })
        discussionTitle?: string,
        @Arg({ isNullable: true })
        closed?: boolean
    ): Promise<string> {

        if (!await ctx.request.isAllowed([ RoleEnum.COACH, RoleEnum.PLAYER ])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        const _discussion = await this.discussionRepository.findOne(discussionId, {
            relations: ['author']
        })
        if (!_discussion) {
            throw new Error(ErrorEnum.NotFound)
        }

        // Member cannot update discussion which wasn't created by himself or is not a COACH
        if (ctx.request.member.role !== RoleEnum.COACH && _discussion.author.id !== ctx.request.member.id) {
            throw new Error(ErrorEnum.Forbidden)
        }

        const partialEntity: QueryDeepPartialEntity<Discussion> = {}
        if (discussionTitle !== null) {
            partialEntity.title = discussionTitle
        }

        if (closed !== null) {
            partialEntity.closed = closed
        }
        try {
            await this.discussionRepository.update(discussionId, partialEntity)
            return "Ok"
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Mutation({ type: String })
    async discussionMessageAdd(
        @Context
        ctx: ContextType,
        @Arg({ type: ID })
        discussionId: number,
        message: string
    ): Promise<string> {
        if (!await ctx.request.isAllowed([ RoleEnum.COACH, RoleEnum.PLAYER ])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        const _discussion = await this.discussionRepository.findOne(discussionId, {
            relations: ['author']
        })
        if (!_discussion) {
            throw new Error(ErrorEnum.NotFound)
        }

        // Cannot add message if the discussion is closed
        if (_discussion.closed) {
            throw new Error(ErrorEnum.BadRequest)
        }

        try {
            const newMessage = new DiscussionMessage()
            newMessage.author = ctx.request.member
            newMessage.discussion = _discussion
            newMessage.message = message
            newMessage.created = new Date()
            await this.discussionMessageRepository.save(newMessage)
            return "Ok"
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Mutation({ type: String })
    async discussionRemove(
        @Context
        ctx: ContextType,
        @Arg({ type: ID })
        discussionId: number
    ): Promise<string> {
        if (!await ctx.request.isAllowed([ RoleEnum.COACH, RoleEnum.PLAYER ])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        const _discussion = await this.discussionRepository.findOne(discussionId, {
            relations: ['author']
        })

        if (!_discussion) {
            throw new Error(ErrorEnum.NotFound)
        }

        // Member cannot remove discussion which wasn't created by himself
        if (_discussion.author.id !== ctx.request.member.id) {
            throw new Error(ErrorEnum.Forbidden)
        }

        try {
            await this.discussionRepository.remove(_discussion)
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }

        return "Ok"
    }
}

export default compileSchema({ roots: [DiscussionSchema] })