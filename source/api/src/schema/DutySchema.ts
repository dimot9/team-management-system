import {Arg, compileSchema, Context, ID, Int, Mutation, Query, SchemaRoot} from "typegql";
import {Container} from "typedi";
import {Context as ContextType} from "graphql-yoga/dist/types";
import {SeasonRepository} from "../repositories/SeasonRepository";
import {ErrorEnum} from "../errors";
import {RoleEnum} from "../enums/RoleEnum";
import {Duty} from "../entities/Duty";
import {DutyRepository} from "../repositories/DutyRepository";
import {DutyInput} from "../entities/inputs/DutyInput";
import isEmptyObject from "graphql-tools/dist/isEmptyObject";
import {DateRangeInput} from "../entities/inputs/DateRangeInput";

@SchemaRoot()
class DutySchema {

    private dutyRepository: DutyRepository = Container.get(DutyRepository)
    private seasonRepository: SeasonRepository = Container.get(SeasonRepository)

    @Query({ type: [Duty] })
    async duties(
        @Context ctx: ContextType,
        @Arg({ type: ID, description: "Season to which duty belongs to." })
        seasonId: number,
        @Arg({ type: DateRangeInput, isNullable: true, description: 'Range of dates in which should duties appear' })
        range: DateRangeInput,
        @Arg({ type: Int, isNullable: true , description: 'Limit result duties count.'})
        limit: number
    ): Promise<Duty[]> {

        if (!await ctx.request.isAllowed([RoleEnum.COACH, RoleEnum.PLAYER])) {
            throw new Error(ErrorEnum.Unauthorized)
        }
        const _season = await this.seasonRepository.findOne(seasonId)
        if (!_season) {
            throw new Error(ErrorEnum.NotFound)
        }

        try {
            return this.dutyRepository.findBySeason(seasonId, range, limit)
        } catch (e) {
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Query({ type: [Duty] })
    async dutiesToday(
        @Context ctx: ContextType,
        @Arg({ type: ID, description: "Season to which duty belongs to." })
        seasonId: number
    ): Promise<Duty[]> {

        if (!await ctx.request.isAllowed([RoleEnum.COACH, RoleEnum.PLAYER])) {
            throw new Error(ErrorEnum.Unauthorized)
        }
        const _season = await this.seasonRepository.findOne(seasonId)
        if (!_season) {
            throw new Error(ErrorEnum.NotFound)
        }

        try {
            return this.dutyRepository.findTodayDuties(seasonId)
        } catch (e) {
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Mutation({ type: String })
    async dutyCreate(
        @Context ctx: ContextType,
        @Arg({ type: DutyInput })
        duty: DutyInput,
        @Arg({ type: ID, description: "Season to which duty belongs to." })
        seasonId: number
    ): Promise<string> {

        if (!await ctx.request.isAllowed([RoleEnum.COACH])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        const _season = await this.seasonRepository.findOne(seasonId)
        if (!_season) {
            throw new Error(ErrorEnum.NotFound)
        }

        try {
            const _duty = new Duty()
            _duty.title = duty.title
            _duty.start = duty.start
            _duty.end = duty.end
            _duty.description = duty.description
            _duty.season = _season
            await this.dutyRepository.save(_duty)

            // Add players to duty
            await this.dutyRepository
                .createQueryBuilder()
                .relation(Duty, 'players')
                .of(_duty)
                .add(duty.players)
            return "Duty successfully created."
        } catch (e) {
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Mutation( { type: String })
    async dutyUpdate(
        @Context ctx: ContextType,
        @Arg({ type: ID! })
        dutyId: number,
        @Arg({ type: DutyInput })
        duty: DutyInput
    ) {

        if (!await ctx.request.isAllowed([ RoleEnum.COACH ])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        const _duty = await this.dutyRepository.findOne(dutyId, { relations: ['players'] })
        if (!_duty) {
            throw new Error(ErrorEnum.NotFound)
        }

        try {
            const {players, ...d} = duty

            if (players !== undefined) {
                // Add players to duty
                await this.dutyRepository
                    .createQueryBuilder()
                    .relation(Duty, 'players')
                    .of(dutyId)
                    .addAndRemove(players, _duty.players)
            }

            if (!isEmptyObject(d)) {
                await this.dutyRepository.update(dutyId, d)
            }
            return "Event updated successfully."
        } catch (e) {
            throw new Error(ErrorEnum.InternalServerError)
        }
    }
}

export default compileSchema({ roots: [DutySchema] })