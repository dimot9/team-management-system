import {Arg, compileSchema, Context, ID, Int, Mutation, Query, SchemaRoot} from "typegql";
import {Container} from "typedi";
import {EventRepository} from "../repositories/EventRepository";
import {Context as ContextType} from "graphql-yoga/dist/types";
import {SeasonRepository} from "../repositories/SeasonRepository";
import {ErrorEnum} from "../errors";
import {Event} from "../entities/Event";
import {RoleEnum} from "../enums/RoleEnum";
import {EventInput} from "../entities/inputs/EventInput";
import {EventComment} from "../entities/EventComment";
import {MemberRepository} from "../repositories/MemberRepository";
import {EventCommentRepository} from "../repositories/EventCommentRepository";
import {EventService} from "../services/EventService";
import Logger from "../loaders/logger";
import {DateRangeInput} from "../entities/inputs/DateRangeInput";

@SchemaRoot()
class EventSchema {

    private eventRepository: EventRepository = Container.get(EventRepository)
    private seasonRepository: SeasonRepository = Container.get(SeasonRepository)
    private memberRepository: MemberRepository = Container.get(MemberRepository)
    private eventCommentRepository: EventCommentRepository = Container.get(EventCommentRepository)
    private eventService: EventService = Container.get(EventService)

    @Query({ type: [Event] })
    async events(
        @Context ctx: ContextType,
        @Arg({ type: ID, description: "Season to which event belongs to." })
        seasonId: number,
        @Arg({ type: DateRangeInput, isNullable: true, description: 'Range of dates in which should events appear' })
        range: DateRangeInput,
        @Arg({ type: Int, isNullable: true , description: 'Limit result events count.'})
        limit: number
    ): Promise<Event[]> {
        if (!await ctx.request.isAllowed([RoleEnum.COACH, RoleEnum.PLAYER])) {
            throw new Error(ErrorEnum.Unauthorized)
        }
        const _season = await this.seasonRepository.findOne(seasonId)
        if (!_season) {
            throw new Error(ErrorEnum.NotFound)
        }

        try {
            return this.eventRepository.findBySeason(seasonId, range, limit)
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Query({ type: Event })
    async event(
        @Context ctx: ContextType,
        @Arg({ type: ID })
            eventId: number
    ) {
        if (!await ctx.request.isAllowed([RoleEnum.COACH, RoleEnum.PLAYER])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        try {
            return await this.eventRepository.findOne(eventId, { relations: ['lineup', 'lineup.member', 'lineup.member.user' ] })
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Mutation({ type: String })
    async eventCreate(
        @Context ctx: ContextType,
        @Arg({ type: EventInput })
        event: EventInput,
        @Arg({ type: ID, description: "Season to which event belongs to." })
        seasonId: number
    ): Promise<string> {

        if (!await ctx.request.isAllowed([RoleEnum.COACH])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        const _season = await this.seasonRepository.findOne(seasonId)
        if (!_season) {
            throw new Error(ErrorEnum.NotFound)
        }

        await this.eventService.createEvent(_season, event)
        return "Event successfully created."
    }

    @Mutation( { type: String })
    async eventUpdate(
        @Context ctx: ContextType,
        @Arg({ type: ID! })
        eventId: number,
        @Arg({ type: EventInput })
        event: EventInput
    ) {

        if (!await ctx.request.isAllowed([ RoleEnum.COACH ])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        const _event = await this.eventRepository.findOne(eventId)
        if (!_event) {
            throw new Error(ErrorEnum.NotFound)
        }

        await this.eventService.updateEvent(eventId, event)
        return "Event updated successfully."
    }

    @Mutation({ type: EventComment })
    async eventAddComment(
        @Context ctx: ContextType,
        @Arg({type: ID!})
        eventId: number,
        @Arg({ type: ID, description: "Member ID - author of the comment"})
        author: number,
        @Arg({ description: "Text of the comment itself"})
        message: string
    ): Promise<EventComment> {

        if (!await ctx.request.isAllowed([ RoleEnum.COACH, RoleEnum.PLAYER ])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        const _author= await this.memberRepository.findOne(author)
        if (!_author) {
            throw new Error(ErrorEnum.NotFound)
        }

        const _event= await this.eventRepository.findOne(eventId)
        if (!_event) {
            throw new Error(ErrorEnum.NotFound)
        }

        try {
            const comment = new EventComment()
            comment.created = new Date()
            comment.author = _author
            comment.message = message
            comment.event = _event
            return await this.eventCommentRepository.save(comment)
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Query({ type: [EventComment] })
    async eventComments(
        @Context ctx: ContextType,
        @Arg({ type: ID, description: "Event where comments belong to." })
        eventId: number
    ): Promise<EventComment[]> {
        if (!await ctx.request.isAllowed([RoleEnum.COACH, RoleEnum.PLAYER])) {
            throw new Error(ErrorEnum.Unauthorized)
        }
        const _event = await this.eventRepository.findOne(eventId)
        if (!_event) {
            throw new Error(ErrorEnum.NotFound)
        }

        try {
            return await this.eventCommentRepository.find({
                relations: ['author', 'author.user'],
                where: {
                    event: eventId
                },
                order:{
                    created: 'DESC'
                }
            })
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Mutation({ type: String })
    async eventSetAbsence(
        @Context
        ctx: ContextType,
        @Arg({ type: ID })
        eventId: number,
        isAbsent: boolean
    ): Promise<string> {

        if (!await ctx.request.isAllowed([RoleEnum.COACH, RoleEnum.PLAYER])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        const _event = await this.eventRepository.findOne(eventId)
        if (!_event) {
            throw new Error(ErrorEnum.NotFound)
        }

        await this.eventService.setAbsence(eventId, ctx.request.member.id, isAbsent)
        return "Ok"
    }

    @Mutation({ type: String })
    async eventRemove(
        @Context
        ctx: ContextType,
        @Arg({ type: ID })
        eventId: number
    ): Promise<string> {
        if (!await ctx.request.isAllowed([RoleEnum.COACH])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        const _event = await this.eventRepository.findOne(eventId)

        if (!_event) {
            throw new Error(ErrorEnum.NotFound)
        }

        await this.eventService.removeEvent(_event)

        return "Ok"
    }
}

export default compileSchema({ roots: [EventSchema] })