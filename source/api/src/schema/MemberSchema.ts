import {Arg, compileSchema, Context, ID, Mutation, Query, SchemaRoot} from 'typegql'
import {Container} from "typedi";
import {MemberRepository} from "../repositories/MemberRepository";
import {Member} from "../entities/Member";
import {MemberInput} from "../entities/inputs/MemberInput";
import {Context as ContextType} from "graphql-yoga/dist/types";
import {ErrorEnum} from "../errors";
import {GQLRoleEnum, RoleEnum} from "../enums/RoleEnum";
import Logger from "../loaders/logger";

@SchemaRoot()
class MemberSchema {

    private memberRepository: MemberRepository = Container.get(MemberRepository)

    @Query({ type: [Member] })
    async members(
        @Context ctx: ContextType,
        @Arg({ type: GQLRoleEnum, isNullable: true, description: 'Members collection can be filtered by member role'})
        memberRole?: RoleEnum
    ): Promise<Member[]> {
        if (!await ctx.request.isAllowed([ RoleEnum.COACH, RoleEnum.PLAYER ])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        try {
            return await this.memberRepository.findAllOrByRole(memberRole)
        } catch (e) {
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Query({ type: [Member] })
    async membersByUser(
        @Context ctx: ContextType,
        @Arg({ type: ID})
        userId: number
    ): Promise<Member[]> {
        if (!ctx.request.user) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        return await this.memberRepository.findByUserId(userId)
    }

    @Query({ type: Member })
    async member(
        @Context ctx: ContextType,
        @Arg({ type: ID })
        memberId: number
    ) {
        if (!await ctx.request.isAllowed([ RoleEnum.COACH, RoleEnum.PLAYER ])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        try {
            return await this.memberRepository.findOne(memberId, {
                relations: ['user']
            })
        } catch (e) {
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Mutation({ type: String })
    async memberUpdate(
        @Context ctx: ContextType,
        @Arg({ type: ID! })
        memberId: number,
        @Arg({ type: MemberInput })
        member: MemberInput
    ): Promise<string> {

        if (!await ctx.request.isAllowed([ RoleEnum.COACH, RoleEnum.PLAYER ])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        // If player is not updating himself (profile), then unauthorized
        if (ctx.request.member.role === RoleEnum.PLAYER && ctx.request.member.id !== +memberId) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        const _member = this.memberRepository.findOne(memberId)
        if (!_member) {
            throw new Error(ErrorEnum.NotFound)
        }

        try {
            await this.memberRepository.update(memberId, member)
            return "Member updated successfully."
        } catch (e) {
            throw new Error(ErrorEnum.InternalServerError)
        }
    }
}

export default compileSchema({ roots: [MemberSchema] })