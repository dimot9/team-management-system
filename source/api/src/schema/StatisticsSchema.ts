import {Arg, compileSchema, Context, ID, Mutation, Query, SchemaRoot} from "typegql";
import {PlayerStatisticsRepository} from "../repositories/PlayerStatisticsRepository";
import {Container} from "typedi";
import {Context as ContextType} from "graphql-yoga/dist/types";
import {PlayerStatistics} from "../entities/PlayerStatistics";
import {RoleEnum} from "../enums/RoleEnum";
import {ErrorEnum} from "../errors";
import Logger from "../loaders/logger";
import {SeasonRepository} from "../repositories/SeasonRepository";
import {PlayerStatisticsInput} from "../entities/inputs/PlayerStatisticsInput";
import {StatisticsService} from "../services/StatisticsService";
import {MemberRepository} from "../repositories/MemberRepository";

@SchemaRoot()
class StatisticsSchema {

    private playerStatisticsRepository: PlayerStatisticsRepository = Container.get(PlayerStatisticsRepository)
    private statisticsService: StatisticsService = Container.get(StatisticsService)
    private seasonRepository: SeasonRepository = Container.get(SeasonRepository)
    private memberRepository: MemberRepository = Container.get(MemberRepository)

    @Query({ type: PlayerStatistics })
    async playerStatistics(
        @Context
        ctx: ContextType,
        @Arg({ type: ID! })
        memberId: number,
        @Arg({ type: ID! })
        seasonId: number
    ): Promise<PlayerStatistics> {

        if (!await ctx.request.isAllowed([RoleEnum.COACH, RoleEnum.PLAYER])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        const _season = await this.seasonRepository.findOne(seasonId)
        if (!_season) {
            throw new Error(ErrorEnum.NotFound)
        }

        try {
            return await this.playerStatisticsRepository.findOne({
                relations: ['season'],
                where: {
                    season: seasonId,
                    player: memberId
                }
            })
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }


    @Mutation({ type: String })
    async playerStatisticsUpdate(
        @Context
            ctx: ContextType,
        @Arg({ type: ID!, description: "Member of Role PLAYER to whom the statistics will be set." })
            memberId: number,
        @Arg({ type: ID! })
            seasonId: number,
        @Arg({ type: PlayerStatisticsInput! })
        playerStatistics: PlayerStatisticsInput
    ): Promise<string> {

        if (!await ctx.request.isAllowed([RoleEnum.COACH])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        const _season = await this.seasonRepository.findOne(seasonId)
        if (!_season) {
            throw new Error(ErrorEnum.NotFound)
        }

        const _member = await this.memberRepository.findOne(memberId)
        if (!_member) {
            throw new Error(ErrorEnum.NotFound)
        }

        if (_member.role === RoleEnum.COACH) {
            throw new Error(ErrorEnum.BadRequest)
        }

        await this.statisticsService.updatePlayerStatistics(_season, _member, playerStatistics)
        return "Ok"
    }
}

export default compileSchema({ roots: [StatisticsSchema] })