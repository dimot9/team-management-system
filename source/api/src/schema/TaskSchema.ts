import {Arg, compileSchema, Context, ID, Mutation, Query, SchemaRoot} from "typegql";
import {Container} from "typedi";
import {Context as ContextType} from "graphql-yoga/dist/types";
import {SeasonRepository} from "../repositories/SeasonRepository";
import {ErrorEnum} from "../errors";
import {RoleEnum} from "../enums/RoleEnum";
import {TaskRepository} from "../repositories/TaskRepository";
import {Task} from "../entities/Task";
import {TaskInput} from "../entities/inputs/TaskInput";
import {TaskToMemberRepository} from "../repositories/TaskToMemberRepository";
import {TaskToMember} from "../entities/TaskToMember";
import {TaskService} from "../services/TaskService";
import Logger from "../loaders/logger";

@SchemaRoot()
class TaskSchema {

    private taskRepository: TaskRepository = Container.get(TaskRepository)
    private taskToMemberRepository: TaskToMemberRepository = Container.get(TaskToMemberRepository)
    private taskService: TaskService = Container.get(TaskService)
    private seasonRepository: SeasonRepository = Container.get(SeasonRepository)

    @Query({ type: [Task] })
    async tasks(
        @Context ctx: ContextType,
        @Arg({ type: ID, description: "Season to which task belongs to." })
        seasonId: number
    ): Promise<Task[]> {

        if (!await ctx.request.isAllowed([RoleEnum.COACH, RoleEnum.PLAYER])) {
            throw new Error(ErrorEnum.Unauthorized)
        }
        const _season = await this.seasonRepository.findOne(seasonId)
        if (!_season) {
            throw new Error(ErrorEnum.NotFound)
        }
        try {
            return await this.taskRepository.findBySeason(seasonId)
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Query({ type: [Task] })
    async myTasks(
        @Context ctx: ContextType,
        @Arg({ type: ID, description: "Season to which task belongs to." })
        seasonId: number
    ): Promise<Task[]> {
        if (!await ctx.request.isAllowed([RoleEnum.COACH, RoleEnum.PLAYER])) {
            throw new Error(ErrorEnum.Unauthorized)
        }
        const _season = await this.seasonRepository.findOne(seasonId)
        if (!_season) {
            throw new Error(ErrorEnum.NotFound)
        }

        try {
            return await this.taskRepository.findBySeasonAndMember(seasonId, ctx.request.member.id)
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Query({ type: Task })
    async task(
        @Context ctx: ContextType,
        @Arg({ type: ID })
        taskId: number
    ): Promise<Task> {

        if (!await ctx.request.isAllowed([RoleEnum.COACH, RoleEnum.PLAYER])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        try {
            return await this.taskRepository.findOne(taskId, {
                relations: ['assignedMembers', 'assignedMembers.member', 'assignedMembers.member.user']
            })
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Mutation({ type: Task })
    async taskCreate(
        @Context ctx: ContextType,
        @Arg({ type: TaskInput })
            task: TaskInput,
        @Arg({ type: ID, description: "Season to which task belongs to." })
            seasonId: number
    ): Promise<Task> {

        if (!await ctx.request.isAllowed([RoleEnum.COACH])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        const _season = await this.seasonRepository.findOne(seasonId)
        if (!_season) {
            throw new Error(ErrorEnum.NotFound)
        }

        return await this.taskService.createTask(_season, task)
    }

    @Mutation( { type: String })
    async taskUpdate(
        @Context ctx: ContextType,
        @Arg({ type: ID! })
            taskId: number,
        @Arg({ type: TaskInput! })
            task: TaskInput
    ): Promise<string> {

        if (!await ctx.request.isAllowed([ RoleEnum.COACH ])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        const _task = await this.taskRepository.findOne(taskId)
        if (!_task) {
            throw new Error(ErrorEnum.NotFound)
        }

        return await this.taskService.updateTask(taskId, task)
    }

    @Mutation({type: Boolean, description: 'Set task as completed or or uncompleted.'})
    async taskCompleted(
        @Context ctx: ContextType,
        @Arg({ type: ID! })
        taskId: number,
        @Arg({ type: ID! })
        memberId: number,
        completed: boolean
    ): Promise<boolean> {

        if (!await ctx.request.isAllowed([RoleEnum.COACH, RoleEnum.PLAYER])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        try {
            await this.taskToMemberRepository
                .createQueryBuilder()
                .update(TaskToMember)
                .set({
                    completed
                })
                .where({
                    taskId,
                    memberId
                })
                .execute()
            return completed
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Mutation({ type: String })
    async taskRemove(
        @Context
        ctx: ContextType,
        @Arg({ type: ID })
        taskId: number
    ): Promise<string> {
        if (!await ctx.request.isAllowed([RoleEnum.COACH])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        const _task = await this.taskRepository.findOne(taskId)

        if (!_task) {
            throw new Error(ErrorEnum.NotFound)
        }

        try {
            await this.taskRepository.remove(_task)
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }

        return "Ok"
    }
}

export default compileSchema({ roots: [TaskSchema] })