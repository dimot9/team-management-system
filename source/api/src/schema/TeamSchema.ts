import {Arg, compileSchema, Context, ID, Mutation, Query, SchemaRoot} from 'typegql'
import {Container} from "typedi";
import {Team} from "../entities/Team";
import {TeamRepository} from "../repositories/TeamRepository";
import {Context as ContextType} from "graphql-yoga/dist/types";
import {ErrorEnum} from "../errors";
import {TeamService} from "../services/TeamService";
import {GQLRoleEnum, RoleEnum} from "../enums/RoleEnum";
import {MemberRepository} from "../repositories/MemberRepository";
import {Member} from "../entities/Member";

@SchemaRoot()
class TeamSchema {

    private teamRepository: TeamRepository = Container.get(TeamRepository)
    private memberRepository: MemberRepository = Container.get(MemberRepository)
    private teamService: TeamService = Container.get(TeamService)

    /**
     * Returns team by id
     * @param id
     */
    @Query({ type: Team })
    async team(
        @Context ctx: ContextType,
        id: number
    ): Promise<Team | undefined> {

        if (!await ctx.request.isAllowed([RoleEnum.COACH, RoleEnum.PLAYER])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        const team = await this.teamRepository.findOne(id)
        if (!team) {
            throw new Error(ErrorEnum.NotFound)
        }
        return team
    }

    /**
     * Creates and returns the new team
     * @param ctx
     * @param name Name of the new created team
     */
    @Mutation({ type: Team })
    async createTeam(
        @Context ctx: ContextType,
        name: string
    ): Promise<Team | undefined> {

        if (!ctx.request.user) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        try {
            return await this.teamService.createTeam(name, ctx.request.user)
        } catch (e) {
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Query({ type: [Member] })
    async teamMembers(
        @Context ctx: ContextType,
        @Arg({ type: ID })
        teamId: number
    ) {
        // you are not logged in
        if (!await ctx.request.isAllowed([RoleEnum.COACH, RoleEnum.PLAYER])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        const _team = await this.teamRepository.findOne(teamId)
        if (!_team) {
            throw new Error(ErrorEnum.NotFound)
        }

        try {
            return await this.memberRepository.findByTeam(_team.id)
        } catch (e) {
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    @Mutation({ type: String })
    async addMember(
        @Context ctx: ContextType,
        @Arg({ type: ID })
        teamId: number,
        @Arg({ type: GQLRoleEnum })
        memberRole: RoleEnum,
        memberNickname: string,
        @Arg({ isNullable: true })
        userEmail?: string
    ): Promise<string> {
        if (!await ctx.request.isAllowed([RoleEnum.COACH])) {
            throw new Error(ErrorEnum.Unauthorized)
        }

        // team was not found, could not proceed
        const _team = await this.teamRepository.findOne(teamId)
        if (!_team) {
            throw new Error(ErrorEnum.NotFound)
        }
        // TODO check if coach, who is adding the member, is also member of this team...just to be sure

        // if userEmail check that member does not already exist
        if (userEmail) {
            const _member = await this.memberRepository.findByTeamAndUserEmail(teamId, userEmail)
            if (_member && _member.role === memberRole) {
                throw new Error(ErrorEnum.MemberAlreadyExists)
            }
        }

        try {
            await this.teamService.addMember(_team, memberRole, memberNickname, userEmail)
        } catch (e) {
            throw new Error(ErrorEnum.InternalServerError)
        }

        return "Member was successfully added."
    }
}

export default compileSchema({ roots: [TeamSchema] })