import {Arg, compileSchema, Context, ID, Mutation, Query, SchemaRoot} from 'typegql'
import {Container} from "typedi";
import {Context as ContextType} from "graphql-yoga/dist/types";
import {ErrorEnum} from "../errors";
import {UserRepository} from "../repositories/UserRepository";
import {User} from "../entities/User";
import {UserInput} from "../entities/inputs/UserInput";

@SchemaRoot()
class UserSchema {

    private userRepository: UserRepository = Container.get(UserRepository)

    @Query({ type: [User] })
    async user(
        @Arg({ type: ID})
        userId: number
    ): Promise<User> {
        // TODO authorization

        return await this.userRepository.findOne(userId)
    }

    @Mutation({ type: String })
    async userUpdate(
        @Context ctx: ContextType,
        @Arg({ type: ID! })
        userId: number,
        @Arg({ type: UserInput })
        user: UserInput
    ): Promise<string> {

        const _user = this.userRepository.findOne(userId)
        if (!_user) {
            throw new Error(ErrorEnum.UserNotFound)
        }

        try {
            await this.userRepository.update(userId ,user)
            return "User updated successfully."
        } catch (e) {
            throw new Error(ErrorEnum.InternalServerError)
        }
    }
}

export default compileSchema({ roots: [UserSchema] })