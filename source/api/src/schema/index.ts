import { mergeSchemas } from 'graphql-tools'

import AuthSchema from './AuthSchema'
import TeamSchema from './TeamSchema'
import MemberSchema from './MemberSchema'
import UserSchema from "./UserSchema";
import EventSchema from "./EventSchema";
import DutySchema from "./DutySchema";
import TaskSchema from "./TaskSchema";
import DiscussionSchema from "./DiscussionSchema";
import StatisticsSchema from "./StatisticsSchema";

export default mergeSchemas({
  schemas: [AuthSchema, TeamSchema, MemberSchema, UserSchema, EventSchema, DutySchema, TaskSchema, DiscussionSchema, StatisticsSchema]
})
