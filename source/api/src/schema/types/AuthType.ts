import { ObjectType, Field } from 'typegql'
import { User } from '../../entities/User'

@ObjectType()
export class AuthType {
    @Field()
    token: string

    @Field({ type: User })
    user: User

    constructor(token: string, user: User) {
        this.token = token
        this.user = user
    }
}