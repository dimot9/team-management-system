import * as JWT from 'jsonwebtoken'
import crypto from 'crypto'
import {Inject, Service} from 'typedi'
import {getRandomString} from "../utils/system";
import {PasswordData, User} from "../entities/User";
import {UserRepository} from "../repositories/UserRepository";
import {ErrorEnum} from "../errors";
import {AuthType} from "../schema/types/AuthType";
import dayjs from "dayjs";
import {mailService, MailServiceInterface} from "./mailService";
import {MemberRepository} from "../repositories/MemberRepository";
import Config from "../loaders/config";

@Service()
export class AuthService {

    @Inject()
    private userRepository: UserRepository

    @Inject()
    private memberRepository: MemberRepository

    private mailService: MailServiceInterface

    public static readonly HASH_ITERATIONS = 500
    public static readonly HASH_LENGTH = 64
    public static readonly HASH_ALGO = 'sha512'

    constructor() {
        this.mailService = mailService
    }

    /**
     * Generates JWT token from user data
     * @param id unique id of the user
     * @param email unique user email
     */
    private generateTokenFromUser(id: number, email: string): string {
        return JWT.sign({ id, email }, Config.token.secret, {
            expiresIn: Config.token.expiration
        })
    }

    /**
     * Generates password salt and hash
     * @param password
     */
    private encryptPassword(password: string): PasswordData {
        const salt = getRandomString(16)
        const hash = crypto.pbkdf2Sync(password, salt, AuthService.HASH_ITERATIONS, AuthService.HASH_LENGTH, AuthService.HASH_ALGO).toString('hex')
        return {
            salt,
            hash
        }
    }

    /**
     * Check plaintext password with original hashed password
     * @param { string } p - plaintext password to compare
     * @param { PasswordData } hashedPassword - existing hashed password i.e salt and hash
     */
    private comparePassword(p: string, hashedPassword: PasswordData): boolean {
        const compareHash = crypto.pbkdf2Sync(p, hashedPassword.salt, AuthService.HASH_ITERATIONS, AuthService.HASH_LENGTH, AuthService.HASH_ALGO).toString('hex')
        return compareHash === hashedPassword.hash
    }

    /**
     * Registers new user
     * @param { string } email - unique email address
     * @param { string } password
     * @param { string }name
     * @param { string } surname
     * @param inviteHash
     */
    async registerUser(email: string, password: string, name: string, surname: string, inviteHash?: string) {

        // check if user with this email already exists
        if (!! await this.userRepository.findByEmail(email)) {
            throw new Error(ErrorEnum.UserExists)
        }

        const user: User = new User()
        user.email = email
        user.name = name
        user.surname = surname
        const hashedPassword: PasswordData = this.encryptPassword(password)
        user.salt = hashedPassword.salt
        user.password = hashedPassword.hash

        // when coach invites a new member without user account. find created member by coach and link him with newly created user
        if (inviteHash) {
            const _member = await this.memberRepository.findByInviteHash(inviteHash)
            if (_member) {
                await this.userRepository.save(user)
                _member.inviteHash = null
                _member.user = user
                await this.memberRepository.save(_member)
                return
            } else {
                throw new Error(ErrorEnum.InviteInvalid)
            }
        }

        await this.userRepository.save(user)
    }

    /**
     * Authenticates user
     * @param email
     * @param password
     */
    async login(email: string, password: string): Promise<AuthType> {

        const _user: User | undefined = await this.userRepository.findByEmail(email)

        if (!!_user) {
            if (this.comparePassword(password, {hash: _user.password, salt: _user.salt})) {
                return new AuthType(
                    this.generateTokenFromUser(_user.id, _user.email),
                    _user
                )
            } else {
                throw new Error(ErrorEnum.InvalidPassword)
            }
        } else {
            throw new Error(ErrorEnum.UserNotFound)
        }
    }

    async forgotPassword(email: string) {

        const _user: User | undefined = await this.userRepository.findByEmail(email)

        if (!!_user) {
            const token = getRandomString(20)
            _user.forgotPasswordToken = token
            _user.forgotPasswordExpiration = dayjs().add(1, "h").toDate()
            await this.userRepository.save(_user)
            await this.mailService.send(
                email,
                "Recover your password",
                `Please, follow this link to recover your password: http://localhost:3000/reset-password/${token}`)
        } else {
            throw new Error(ErrorEnum.UserNotFound)
        }
    }

    async recoveryPassword(token: string, password: string) {
        const _user: User = await this.userRepository.findUserByToken(token)

        if (_user) {
            const hashedPassword: PasswordData = this.encryptPassword(password)
            _user.salt = hashedPassword.salt
            _user.password = hashedPassword.hash
            _user.forgotPasswordToken = null
            _user.forgotPasswordExpiration = null

            await this.userRepository.save(_user)
            await this.mailService.send(_user.email, "Password recovered", "Your password was successfully changed. Now, you can sign in with your new password.")
        } else {
            throw new Error(ErrorEnum.ForgotPasswordTokenInvalid)
        }
    }
}