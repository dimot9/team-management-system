import {Inject, Service} from 'typedi'
import {UserRepository} from '../repositories/UserRepository'
import {AuthService} from './AuthService';

@Service()
export class DataFixturesService {
  @Inject()
  authService: AuthService;

  @Inject()
  userRepository: UserRepository;

  // TODO
}
