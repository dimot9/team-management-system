import {Inject, Service} from "typedi";
import {ErrorEnum} from "../errors";
import {Season} from "../entities/Season";
import isEmptyObject from "graphql-tools/dist/isEmptyObject";
import {EventRepository} from "../repositories/EventRepository";
import {EventLineupRepository} from "../repositories/EventLineupRepository";
import {EventInput} from "../entities/inputs/EventInput";
import {Event} from "../entities/Event";
import Logger from "../loaders/logger";
import {EventLineup} from "../entities/EventLineup";

@Service()
export class EventService {

    @Inject()
    private eventRepository: EventRepository

    @Inject()
    private eventLineupRepository: EventLineupRepository

    /**
     * Create new event. Add lineup to the event.
     * @param season
     * @param event
     */
    async createEvent(season: Season, event: EventInput): Promise<void> {
        try {
            const _event = new Event()
            _event.title = event.title
            _event.type = event.type
            _event.allDay = event.allDay
            _event.start = event.start
            _event.end = event.end
            _event.meetingTime = event.meetingTime
            _event.notes = event.notes
            _event.place = event.place
            _event.season = season
            await this.eventRepository.save(_event)

            // Add players to event's lineup
            if (event.lineup && event.lineup.length > 0) {
                await this.eventLineupRepository.insert(event.lineup.map((memberId: number) => {
                    return {
                        memberId,
                        eventId: _event.id
                    }
                }))
            }
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    /**
     * Update event. Update lineup too.
     * @param eventId
     * @param event
     */
    async updateEvent(eventId: number, event: EventInput): Promise<void> {
        try {
            const {lineup, ...ev} = event
            if (lineup !== undefined) {
                // Add players to event's lineup
                await this.eventLineupRepository.delete({eventId})
                await this.eventLineupRepository.insert(lineup.map((memberId: number) => {
                    return {
                        memberId,
                        eventId
                    }
                }))
            }

            if (!isEmptyObject(ev)) {
                await this.eventRepository.update(eventId, ev)
            }
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    async setAbsence(eventId: number, memberId: number, isAbsent: boolean) {
        try {
            const _eventLineupRelation = await this.eventLineupRepository.find({
                where: {
                    eventId,
                    memberId
                }
            })

            if (_eventLineupRelation.length > 0) {
                await this.eventLineupRepository
                    .createQueryBuilder()
                    .update(EventLineup)
                    .set({
                        isAbsent
                    })
                    .where({
                        eventId,
                        memberId
                    })
                    .execute()
            }

        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    async removeEvent(event: Event) {
        try {
            await this.eventRepository.remove(event)
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }
}