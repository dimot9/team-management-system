import {Inject, Service} from "typedi";
import {PlayerStatisticsRepository} from "../repositories/PlayerStatisticsRepository";
import {SeasonRepository} from "../repositories/SeasonRepository";
import {PlayerStatisticsInput} from "../entities/inputs/PlayerStatisticsInput";
import Logger from "../loaders/logger";
import {ErrorEnum} from "../errors";
import {PlayerStatistics} from "../entities/PlayerStatistics";
import {Season} from "../entities/Season";
import {Member} from "../entities/Member";

@Service()
export class StatisticsService {

    @Inject()
    private playerStatisticsRepository: PlayerStatisticsRepository

    @Inject()
    private seasonRepository: SeasonRepository

    async updatePlayerStatistics(season: Season, member: Member, playerStatistics: PlayerStatisticsInput): Promise<void> {
        try {
            if (playerStatistics.id) {
                await this.playerStatisticsRepository.update(playerStatistics.id, playerStatistics)
            } else {
                const stats = new PlayerStatistics()
                stats.player = member
                stats.season = season
                stats.assists = playerStatistics.assists
                stats.goalsScored = playerStatistics.goalsScored
                stats.minutesPlayed = playerStatistics.minutesPlayed

                await this.playerStatisticsRepository.save(stats)
            }
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }
}