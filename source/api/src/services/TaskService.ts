import {Inject, Service} from "typedi";
import {TaskRepository} from "../repositories/TaskRepository";
import {TaskToMemberRepository} from "../repositories/TaskToMemberRepository";
import {TaskInput} from "../entities/inputs/TaskInput";
import {Task} from "../entities/Task";
import {ErrorEnum} from "../errors";
import {Season} from "../entities/Season";
import isEmptyObject from "graphql-tools/dist/isEmptyObject";
import Logger from "../loaders/logger";

@Service()
export class TaskService {

    @Inject()
    private taskRepository: TaskRepository

    @Inject()
    private taskToMemberRepository: TaskToMemberRepository

    /**
     * Create new task. Add assigned players to the task.
     * @param season
     * @param task - new task data
     */
    async createTask(season: Season, task: TaskInput): Promise<Task> {

        try {
            const _task = new Task()
            _task.name = task.name
            _task.description = task.description
            _task.season = season
            await this.taskRepository.save(_task)

            if (task.members?.length > 0) {
                // Add players to task
                await this.taskToMemberRepository.insert(task.members.map((memberId: number) => {
                    return {
                        memberId,
                        taskId: _task.id
                    }
                }))
            }
            return _task
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }

    /**
     * Update task. Update assigned members too.
     * @param taskId
     * @param task - new task data
     */
    async updateTask(taskId: number, task: TaskInput): Promise<string> {

        try {
            const {members, ...tsk} = task
            if (members !== undefined) {
                // assign members to task
                await this.taskToMemberRepository.delete({taskId})
                await this.taskToMemberRepository.insert(members.map((memberId: number) => {
                    return {
                        memberId,
                        taskId
                    }
                }))
            }

            if (!isEmptyObject(tsk)) {
                await this.taskRepository.update(taskId, tsk)
            }

            return "ok"
        } catch (e) {
            Logger.error(e)
            throw new Error(ErrorEnum.InternalServerError)
        }
    }
}