import {Inject, Service} from "typedi";
import {User} from "../entities/User";
import {Member} from "../entities/Member";
import {RoleEnum} from "../enums/RoleEnum";
import {TeamRepository} from "../repositories/TeamRepository";
import {Team} from "../entities/Team";
import {MemberRepository} from "../repositories/MemberRepository";
import {Season} from "../entities/Season";
import {SeasonRepository} from "../repositories/SeasonRepository";
import {UserRepository} from "../repositories/UserRepository";
import {mailService, MailServiceInterface} from "./mailService";
import {getRandomString} from "../utils/system";

@Service()
export class TeamService {

    @Inject()
    private teamRepository: TeamRepository

    @Inject()
    private memberRepository: MemberRepository

    @Inject()
    private seasonRepository: SeasonRepository

    @Inject()
    private userRepository: UserRepository

    private mailService: MailServiceInterface

    constructor() {
        this.mailService = mailService
    }

    /**
     * Creates new team and COACH of this team
     * @param name Name of the created team
     * @param user User which creates this team and becomes its COACH
     */
    async createTeam(name: string, user: User): Promise<Team | undefined> {
        // create first season for the team
        const season = new Season()
        season.name = 'First season'
        season.start = new Date()

        const team = new Team()
        team.name = name
        team.currentSeason = season
        team.seasons = [season]

        await this.seasonRepository.save(season)
        await this.teamRepository.save(team)

        const member = new Member()
        member.user = user
        member.role = RoleEnum.COACH
        member.team = team
        await this.memberRepository.save(member)

        return team
    }

    /**
     * Creates new member and adds him to the team - if user email is provided, then also connect him to the user
     * @param team
     * @param memberRole
     * @param memberNickname
     * @param userEmail
     * @param userName
     * @param userSurname
     */
    async addMember(team: Team, memberRole: RoleEnum, memberNickname: string, userEmail?: string): Promise<void> {

        // create new member and add him to the team
        let _member = new Member()
        _member.role = memberRole
        _member.team = team
        _member.nickname = memberNickname
        _member = await this.memberRepository.save(_member)

        if (userEmail) {
            const _user = await this.userRepository.findByEmail(userEmail)

            // if account with this email does not exist, send link to registration form to this email, with member id for connecting with member entity after registration
            if (!_user) {

                const inviteHash = getRandomString(20)
                _member.inviteHash = inviteHash
                await this.memberRepository.save(_member)

                await this.mailService.send(
                    userEmail,
                    `TMS - Invitation to team ${team.name}`,
                    `Welcome,\nyou have been invited to the team ${team.name} by your coach.\nPlease, follow this link to connect with your team: http://localhost:3000/register/${inviteHash}?email=${userEmail}`)
                return
            } else {
                await this.mailService.send(
                    userEmail,
                    `TMS - Invitation to team ${team.name}`,
                    `Hi, ${_user.name},\nyou have been invited to the team ${team.name} by your coach.`)
            }

            // connect member with user entity if exists
            _member.user = _user
            await this.memberRepository.save(_member)
        }
    }
}