import * as nodemailer from 'nodemailer'
import {Transporter} from "nodemailer";
import SMTPTransport from "nodemailer/lib/smtp-transport";
import Logger from "../loaders/logger";
import Config from "../loaders/config";

export interface MailServiceInterface {
    send(to: string, subject: string, message: string): void
}

class MailService implements MailServiceInterface {

    private transporter: Transporter
    private smtpOptions: SMTPTransport.Options

    constructor() {
        this.smtpOptions = Config.smtp
        this.transporter = nodemailer.createTransport(this.smtpOptions)
    }

    async send(to: string, subject: string, message: string) {
        try {
            await this.transporter.sendMail({
                from: 'noreply@tms.com',
                to,
                subject,
                text: message
            })
        } catch (e) {
            Logger.error(`Failed to send email to: ${to}.`)
            throw new Error("Failed to send email.")
        }
    }
}

export const mailService = new MailService();