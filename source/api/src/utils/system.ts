import crypto from 'crypto'

/**
 * Creates promise which executes timeout for given ms
 * @function
 * @param {number} ms - Length of the timeout
 */
const sleep = (ms: number) => {
  return new Promise((res: any) => {
    setTimeout(res, ms)
  })
}

/**
 * Generates random string of characters i.e salt
 * @function
 * @param { number } length - Length of the random string
 */
const getRandomString = (length: number) => {
  return crypto.randomBytes(Math.ceil(length/2))
      .toString('hex') /** convert to hexadecimal format */
      .slice(0,length);   /** return required number of characters */
}

export {
  sleep,
  getRandomString
}
