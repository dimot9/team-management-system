import test from "ava";
import gql from 'graphql-tag'
import queryInjector from "./helpers/queryInjector";

export function runAuthTests() {
    test.serial('login - user not found', async t => {
        const { data, errors } = await queryInjector(gql`
            mutation {
                login(email: "test@test.cz", password: "password") {
                    token
                }
            }
        `)

        t.is(errors[0].message, 'UserNotFound')
        t.snapshot(data, "Data should be null")
    })

    test.serial('register - name and surname required', async t => {
        const { data, errors } = await queryInjector(gql`
            mutation {
                register(email: "test@test.cz", password: "password")
            }
        `)

        t.is(errors.length, 2)
        t.snapshot(data, "Data should be null")
    })

    test.serial('register - success', async t => {
        const { data, errors } = await queryInjector(gql`
            mutation {
                register(email: "test@test.cz", name: "John", surname: "Snow", password: "password")
            }
        `)

        t.is(errors, undefined)
        t.snapshot(data.register)
    })

    test.serial('login - InvalidPassword', async t => {
        const { data, errors } = await queryInjector(gql`
            mutation {
                login(email: "test@test.cz", password: "wrongPassword") {
                    token
                }
            }
        `)

        t.is(errors[0].message, 'InvalidPassword')
        t.snapshot(data, "Data should be null")
    })

    test.serial('login - success', async t => {
        const { data, errors } = await queryInjector(gql`
            mutation {
                login(email: "test@test.cz", password: "password") {
                    user {
                        fullname
                    }
                }
            }
        `)

        t.is(errors, undefined)
        t.snapshot(data.login)
    })

    test.serial('register - user exists', async t => {
        const { data, errors } = await queryInjector(gql`
            mutation {
                register(email: "test@test.cz", name: "John", surname: "Snow", password: "password")
            }
        `)

        t.is(errors[0].message, 'UserExists')
        t.snapshot(data, "Data should be null")
    })
}
