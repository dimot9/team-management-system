import {RoleEnum} from "../../src/enums/RoleEnum";
import {Container} from "typedi";
import {UserRepository} from "../../src/repositories/UserRepository";
import {User} from "../../src/entities/User";

export function mockIsAllowedFunction(expectedRoles: RoleEnum[]): Function {
    return function (roles: RoleEnum[]): boolean {
        expectedRoles.forEach(r => {
            if (!roles.includes(r)) {
                return false
            }
        })
        return true
    }
}

export async function mockUser(email: string = 'user@test.cz') {
    const userRepository: UserRepository = Container.get(UserRepository)
    const user: User = new User()
    user.email = email
    user.name = 'John'
    user.surname = 'Snow'
    user.password = 'password'
    user.salt = 'salt'
    return await userRepository.save(user)
}