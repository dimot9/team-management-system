import { graphql } from 'graphql';
import { print } from 'graphql/language/printer';
import schema from '../../src/schema';

export default async (query: any, context?: any): Promise<any> => {
    const queryString = typeof query === 'string' ? query : print(query);
    const value = await graphql(schema, queryString, undefined, context ?? {request: {}});
    return value;
}