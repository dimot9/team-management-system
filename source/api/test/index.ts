import {runAuthTests} from "./authTests";
import {runTeamTests} from "./teamTests";
import test from "ava";
import database from "../src/loaders/database";
import {getConnection} from "typeorm";

test.before(async () => {
    await database()
})

runAuthTests()
runTeamTests()

test.after(async () => {
    await getConnection("default").close()
})