import test, {ExecutionContext} from "ava";
import gql from 'graphql-tag'
import queryInjector from "./helpers/queryInjector";
import {Context as ContextType} from "graphql-yoga/dist/types";
import {mockUser} from "./helpers/contextResolver";
import {Container} from "typedi";
import {TeamService} from "../src/services/TeamService";

export function runTeamTests() {

    test.serial.before(async (t:ExecutionContext<ContextType>) => {
        // mock user
        const testUser = await mockUser()
        t.context = {
            request: {
                user: testUser
            }
        }
    })

    test.serial('create team - unauthorized', async (t:ExecutionContext<ContextType>) => {
        const { data, errors } = await queryInjector(gql`
            mutation {
                createTeam(name: "Test Team") {
                    name
                }
            }
        `)

        t.is(errors[0].message, 'Unauthorized')
        t.snapshot(data, "Data should be null")
    })

    // unit test of createTeam function in TeamService
    test.serial('create team - success', async (t:ExecutionContext<ContextType>) => {

        // set test variables
        const TEST_TEAM_NAME = 'Test Team'

        // get TeamService with DI
        const teamService: TeamService = Container.get(TeamService)

        // tested function
        const createdTeam = await teamService.createTeam(TEST_TEAM_NAME, t.context.request.user)

        // Team with name <TEST_TEAM_NAME> created
        t.is(createdTeam.name, TEST_TEAM_NAME)
        t.assert(createdTeam.seasons.length > 0, "Default season for the team created." )
    })
}
