export default function ({ app, store }) {

    return {
        httpEndpoint: 'http://localhost:4000',
        browserHttpEndpoint: '/api',
    }
}