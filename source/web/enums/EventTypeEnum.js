export const EventTypeEnum = {
    TRAINING: 'TRAINING',
    MATCH: 'MATCH',
    TOURNAMENT: 'TOURNAMENT',
    OTHER: 'OTHER'
}