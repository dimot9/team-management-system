export default async function ({ app, error, store, redirect, route }) {
    const routeName = app.getRouteBaseName(route)
    if (routeName !== 'app' && store.state.team.team === null) {
        redirect(app.localePath({ name: 'app' }))
    }
}