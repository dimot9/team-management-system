export default function ({app, error, store, route, redirect}) {
    const hasToken = !!app.$apolloHelpers.getToken()
    store.commit('user/hasToken', hasToken)
    const routeName = route.name
    if (routeName?.startsWith("app")) {
        if (!hasToken) {
            redirect(app.localePath({name: 'login'}))
        }
    }

    if (routeName === 'login') {
        if (hasToken) {
            redirect(app.localePath({ name: 'app' }))
        }
    }

}