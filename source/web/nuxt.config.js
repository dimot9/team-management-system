require('dotenv').config()

module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/svg+xml', href: '/images/team.svg' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/css/tailwind.css',
    '@fortawesome/fontawesome-svg-core/styles.css',
      '@fullcalendar/core/main.css',
      '@fullcalendar/daygrid/main.css',
      '@fullcalendar/timegrid/main.css',
      'epic-spinners/dist/lib/epic-spinners.min.css'
  ],

  tailwindcss: {
    cssPath: '~/assets/css/tailwind.css',
  },
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/fontawesome.js',
    { src: '~/plugins/full-calendar.js', ssr: false },
    '~/plugins/api-request.js'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
  ],
  // dotenv options
  dotenv: {
    path: '../../' // point to global .env file
  },

  eslint: {
    fix: true
  },

  router: {
    middleware: ['auth']
  },

  proxy: {
    '/api': {
      target: 'http://localhost:4000',
      pathRewrite: {'^/api': '/'}
    },
    '/api/playground': {
      target: 'http://localhost:4000/playground',
      pathRewrite: {'^/api': '/'}
    }
  },
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/apollo',
    '@nuxtjs/proxy',
    '@nuxtjs/toast',
    [
      'nuxt-i18n',
      {
        defaultLocale: 'en',
        locales: [
          { code: 'cs', iso: 'cs-CZ', file: 'cs.json'},
          { code: 'en', iso: 'en-Us', file: 'en.json'}
        ],
        lazy: true,
        langDir: 'translations/',
        parsePages: false,
        pages: {
          about: {
            cs: '/o-aplikaci',
            en: '/about'
          },
          app: {
            cs: '/app',
            en: '/app',
          },
          'app/dashboard': {
            cs: '/app/nastenka',
            en: '/app/dashboard'
          },
          'app/calendar': {
            cs: '/app/kalendar',
            en: '/app/calendar'
          },
          'app/_event/index': {
            cs: '/app/udalost/:event?',
            en: '/app/event/:event?'
          },
          'app/tasks': {
            cs: '/app/ukoly',
            en: '/app/tasks'
          },
          'app/_task/index': {
            cs: '/app/ukol/:task?',
            en: '/app/task/:task?'
          },
          'app/team': {
            cs: '/app/tym',
            en: '/app/team'
          },
          'app/_member/index': {
            cs: '/app/clen/:member?',
            en: '/app/member/:member?'
          },
          'app/discussions': {
            cs: '/app/diskuze',
            en: '/app/discussions'
          },
          'app/_discussion/index': {
            cs: '/app/diskuze/:discussion?',
            en: '/app/discussion/:discussion?'
          },
          login: {
            cs: '/prihlaseni',
            en: '/login'
          },
          'resetPassword/_token': {
            cs: '/obnoveni-hesla/:token?',
            en: '/reset-password/:token?'
          },
          'registration/_inviteHash': {
            cs: '/registrace/:inviteHash?',
            en: '/register/:inviteHash?'
          },
        }
      }
    ]
  ],
  // Apollo config
  apollo: {
    tokenName: 'apollo-token',
    cookieAttributes: {
      secure: process.env.ENV !== 'dev',
      expires: 1,// cookie expiration 1 day
      path: '/'
    },
    clientConfigs: {
      default: '~/apollo/default-config.js'
    },
    defaultOptions: {
      $query: {
        fetchPolicy: 'network-only'
      }
    },
    errorHandler: '~/plugins/apollo-error-handler.js'
  },

  toast: {
    position: 'top-right',
    duration: 5000,
    action: {
      text: 'X',
      onClick : (e, toastObject) => {
        toastObject.goAway(0);
      },
      class: 'notification'
    },
    containerClass: 'theme-light',
    className: 'notification'
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
      }
    }
  }
}
