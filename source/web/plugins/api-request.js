class ApiRequest {
    constructor(apollo, toast, store) {
        this.apollo = apollo
        this.toast = toast
        this.store = store
    }

    async mutate({
        mutation,
        variables
    }) {
        try {
            const mutationName = mutation.definitions[0].name.value
            const result = await this.apollo.mutate({
                mutation,
                variables,
                context: {
                    headers: {
                        'X-tms-member': this.store.state.user.member ? this.store.state.user.member.id : null
                    }
                }
            }).then(({data}) => data && data[mutationName])
            return result
        } catch (e) {
            console.log(e)
            if (e.networkError) {
                if (e.networkError.statusCode && e.networkError.statusCode === 401) {
                    const {key} = e.networkError.result

                    if (key === 'tokenExpired') {
                        if (this.store.state.user.hasToken) {
                            await this.store.dispatch('user/logout')
                            this.store.commit('lockToggle')
                            throw new Error('tokenExpired')

                        }
                    }
                } else if (e.networkError.result?.errors) {
                    throw new Error(e.networkError.result.errors[0].message)
                }
            }
            if (e.graphQLErrors?.length > 0) {
                throw new Error(e.graphQLErrors[0].key)
            }
        }
    }

    async query({
        query,
        variables
    }) {
        try {
            const queryName = query.definitions[0].name.value
            return await this.apollo.query({
                query,
                variables,
                context: {
                    headers: {
                        'X-tms-member': this.store.state.user.member ? this.store.state.user.member.id : null,
                        'time': new Date().getTime()
                    }
                }
            }).then(({ data }) => data && data[queryName])
        } catch (e) {
            if (e.networkError) {
                if (e.networkError.statusCode && e.networkError.statusCode === 401) {
                    const {key} = e.networkError.result

                    if (key === 'tokenExpired') {
                        if (this.store.state.user.hasToken) {
                            await this.store.dispatch('user/logout')
                            this.store.commit('lockToggle')
                            throw new Error('tokenExpired')

                        }
                    }
                } else if (e.networkError.result?.errors) {
                    throw new Error(e.networkError.result.errors[0].message)
                }
            }
            if (e.graphQLErrors?.length > 0) {
                throw new Error(e.graphQLErrors[0].key)
            }
        }
    }
}

export default ({ app, store }, inject) => {
    inject('api', new ApiRequest(app.apolloProvider.defaultClient, app.$toast, store ))
}