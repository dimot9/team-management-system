export default async ({ graphQLErrors, networkError, operation, forward }, { app, store }) => {
    if (networkError) {
        if (networkError.statusCode && networkError.statusCode === 401) {
            const { key } = networkError.result

            if (key === 'tokenExpired') {
                if (store.state.user.hasToken) {
                    await store.dispatch('user/logout')
                    store.commit('lockToggle')
                    app.$toast?.error(app.i18n.t('error.tokenExpired'))
                }
            }
        } else if (networkError.result?.errors) {
            networkError.result.errors.forEach(err => {
                app.$toast?.error(err.message)
            })
        }
    }
    if (graphQLErrors?.length > 0) {
        const err = graphQLErrors[0]
        app.$toast?.error(app.i18n.t(`error.${err.key}`))
    }
}