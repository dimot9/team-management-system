import Vue from 'vue'
import { library, config } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {
    faSignOutAlt,
    faBars,
    faTimes,
    faUsers,
    faComments,
    faCalendarAlt,
    faTasks,
    faTachometerAlt,
    faUsersCog,
    faLock,
    faLockOpen,
    faCheck,
    faPlus,
    faEdit,
    faArrowAltCircleRight,
    faTimesCircle,
    faCheckCircle,
    faChartPie,
    faTrash,
    faUserInjured,
    faRunning,
    // wysiwyg
    faBold,
    faItalic,
    faStrikethrough,
    faUnderline,
    faList,
    faListOl,
    faUndo,
    faRedo,
    faLink,
    faComment
} from '@fortawesome/free-solid-svg-icons'

// This is important, we are going to let Nuxt.js worry about the CSS
config.autoAddCss = false

// You can add your icons directly in this plugin. See other examples for how you
// can add other styles or just individual icons.
library.add(
    faSignOutAlt,
    faBars,
    faTimes,
    faUsers,
    faComments,
    faCalendarAlt,
    faTasks,
    faTachometerAlt,
    faUsersCog,
    faLock,
    faLockOpen,
    faCheck,
    faPlus,
    faEdit,
    faBold,
    faItalic,
    faStrikethrough,
    faUnderline,
    faList,
    faListOl,
    faUndo,
    faRedo,
    faLink,
    faComment,
    faArrowAltCircleRight,
    faTimesCircle,
    faCheckCircle,
    faChartPie,
    faTrash,
    faUserInjured,
    faRunning
)

// Register the component globally
Vue.component('font-awesome-icon', FontAwesomeIcon)