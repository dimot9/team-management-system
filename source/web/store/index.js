export const state = () => ({
    loginOpen: false,
    registerOpen: false,
    lockOpen: false,
    forgotPasswordOpen: false
})

export const mutations = {
    loginToggle(state, open) {
        state.loginOpen = open
    },
    registerToggle(state) {
        state.registerOpen = !state.registerOpen
    },
    lockToggle(state) {
        state.lockOpen = !state.lockOpen
    },
    forgotPasswordToggle(state) {
        state.forgotPasswordOpen = !state.forgotPasswordOpen
    }
}

export const actions = {
    async nuxtServerInit({ dispatch }, { app, redirect }) {
        try {
            await dispatch('user/fetchUser')
        } catch (e) {
            if (e.message === 'tokenExpired') {
                redirect(app.localePath({ name: 'login' }))
            }
        }

    }
}
