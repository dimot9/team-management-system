export const state = () => ({
    team: null,
    season: null
})

export const mutations = {
    team(state, team) {
        state.team = team
    },
    season(state, season) {
        state.season = season
    }
}
