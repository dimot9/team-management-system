import gql from 'graphql-tag'
import {RoleEnum} from "../enums/RoleEnum";

export const state = () => ({
    user: null,
    member: null,
    hasToken: false
})

export const mutations = {
    user(state, user) {
        state.user = user
    },
    member(state, member) {
        state.member = member
    },
    hasToken(state, hasToken) {
        state.hasToken = hasToken
    }
}

export const actions = {
    async fetchUser({ commit, dispatch }) {
        try {
            const apollo = this.app.apolloProvider.defaultClient
            const { id, name, surname, email } = await apollo.query({
                query: gql`
                    query {
                        me {
                            id
                            name
                            surname
                            email
                        }
                    }
                `
            }).then(({ data }) => data && data.me)
            commit('user', { id, name, surname, email })
        } catch (e) {
            if (e.networkError) {
                if (e.networkError.statusCode && e.networkError.statusCode === 401) {
                    const { key } = e.networkError.result

                    if (key === 'tokenExpired') {
                        throw new Error('tokenExpired')
                    }
                }
            }
        }

    },
    async login({ commit }, token) {
        await this.app.$apolloHelpers.onLogin(token)
        commit('hasToken', !!this.app.$apolloHelpers.getToken())
    },
    async logout({ commit }) {
        await this.app.$apolloHelpers.onLogout()
        commit('hasToken', !!this.app.$apolloHelpers.getToken())
    }
}

export const getters = {
    isCoach(state) {
        return state.member?.role === RoleEnum.COACH
    }
}
